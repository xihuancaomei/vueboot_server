package com.asianrapid.service.impl.sys;



import com.asianrapid.dao.PlantDao;
import com.asianrapid.model.sys.PlantEntity;
import com.asianrapid.service.sys.PlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("plantServiceImpl")
public class PlantServiceImpl implements PlantService {
	@Autowired
	private PlantDao plantDao;


	@Override
	public List<PlantEntity> plantsList(int pageSize, String plantType, String plantName, int start) {
		return plantDao.plantsList(pageSize, plantType, plantName, start);
	}

	@Override
	public Integer plantsSize(int pageSize, String plantType, String plantName, int start) {
		return plantDao.plantsSize(pageSize,plantType,plantName,start);
	}

	@Override
	public void insertPlant(PlantEntity plantEntity) {
		plantDao.insertPlant(plantEntity);
	}

	@Override
	public void updatePlant(PlantEntity plantEntity) {
		plantDao.updatePlant(plantEntity);
	}

	@Override
	public void deletePlants(List<String> groupId) {
		plantDao.deletePlants(groupId);
	}

	@Override
	public List<PlantEntity> plantTypeList() {
		return plantDao.plantTypeList();
	}
}