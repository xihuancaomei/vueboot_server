package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.GameDao;
import com.asianrapid.model.sys.GameEntity;
import com.asianrapid.service.sys.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr lx
 * @description
 * @date 2020/9/3
 */
@Service(value = "gameServiceImpl")
public class GameServiceImpl implements GameService {

    @Autowired
    private GameDao gameDao;


    @Override
    public List<GameEntity> gamesList(String gameName,String gameType,int pageSize, int start) {
        return gameDao.gamesList(gameName,gameType, pageSize, start);
    }

    @Override
    public Integer gamesSize(String gameName,String gameType,int pageSize, int start) {
        return gameDao.gamesSize(gameName,gameType, pageSize, start);
    }

    @Override
    public void insertGame(GameEntity gameEntity) {
        gameDao.insertGame(gameEntity);
    }

    @Override
    public void updateGame(GameEntity gameEntity) {
        gameDao.updateRole(gameEntity);
    }

    @Override
    public void deleteGames(List<String> groupId) {
        gameDao.deleteRoles(groupId);
    }

    @Override
    public List<GameEntity> allGames() {
        return null;
    }

    @Override
    public GameEntity getGameEntityByGameName(String gameName) {
        return gameDao.getGameEntityByGameName(gameName);
    }

    @Override
    public List<GameEntity> gameTypeList() {
        return gameDao.gameTypeList();
    }
}
