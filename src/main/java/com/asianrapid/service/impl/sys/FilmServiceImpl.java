package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.FilmDao;
import com.asianrapid.model.sys.Film;
import com.asianrapid.service.sys.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gx
 * @description
 * @date 2020/9/3
 */
@Service(value = "filmServiceImpl")
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmDao filmDao;

    @Override
    public List<Film> filmList(int pageSize, int start,String type,String name) {
        return filmDao.filmList(pageSize,start,type,name);
    }

    @Override
    public Integer filmSize(int pageSize, int start,String type,String name) {
        return filmDao.filmSize(pageSize,start,type,name);
    }

    @Override
    public void insertFilm(Film filmEntity) {
        filmDao.insertFilm(filmEntity);
    }

    @Override
    public List<Film> allFilm() {
        return filmDao.allFilm();
    }

    @Override
    public void updateFilm(Film filmEntity) {
        filmDao.updateFilm(filmEntity);
    }

    @Override
    public void deleteFilm(List<String> groupId) {
        filmDao.deleteFilm(groupId);
    }

    @Override
    public List<Film> filmByType() {
        return filmDao.filmByType();
    }
}
