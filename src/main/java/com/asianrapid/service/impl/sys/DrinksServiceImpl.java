package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.DrinksDao;
import com.asianrapid.model.sys.DrinksEntity;
import com.asianrapid.service.sys.DrinksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @autohr lzd
 * @description
 * @date 2020/9/9
 */
@Service
public class DrinksServiceImpl implements DrinksService {
    @Autowired
    private DrinksDao drinksDao;
    @Override
    public ArrayList<DrinksEntity> getDrinksList(String drinksCapacity, String drinksType, String drinksName, int pageSize, int start) {
        return drinksDao.getDrinksList(drinksCapacity, drinksType, drinksName, pageSize, start);
    }

    @Override
    public Integer getDrinksListSize(String drinksCapacity, String drinksType, String drinksName, int pageSize, int start) {
        return drinksDao.getDrinksListSize(drinksCapacity, drinksType, drinksName, pageSize, start);
    }

    @Override
    public List<DrinksEntity> getDrinksType() {
        return drinksDao.getDrinksType();
    }

    @Override
    public List<DrinksEntity> getDrinksCapacity() {
        return drinksDao.getDrinksCapacity();
    }

    @Override
    public void insertDrinks(DrinksEntity drinksEntity) {
        drinksDao.insertDrinks(drinksEntity);
    }

    @Override
    public void deleteDrinksById(List<String> groupId) {
        drinksDao.deleteDrinksById(groupId);
    }

    @Override
    public void updateDrinksById(DrinksEntity drinksEntity) {
        drinksDao.updateDrinksById(drinksEntity);
    }
}
