package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.CustomerDao;
import com.asianrapid.model.sys.Customer;
import com.asianrapid.model.sys.CustomerOrderDto;
import com.asianrapid.model.sys.Order;
import com.asianrapid.model.sys.QueryVo;
import com.asianrapid.service.sys.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public List<String> selectOrderNameList() {
        return customerDao.selectOrderNameList();
    };

    @Override
    public List<CustomerOrderDto> customersList(String customerName, String orderId, String name, int pageSize, int start) {
        return customerDao.customersList(customerName, orderId, name, pageSize, start);
    }

    @Override
    public Integer customerSize(String customerName, String orderId, String name, int pageSize, int start) {
        return customerDao.customerSize(customerName, orderId, name, pageSize, start);
    }

    @Override
    public void insertCustomer(CustomerOrderDto customerOrderDto) {
        customerDao.insertCustomer(customerOrderDto);
    }

    @Override
    public void insertOrder(CustomerOrderDto customerOrderDto) {
        customerDao.insertOrder(customerOrderDto);
    }

    @Override
    public Long selectCustomerId() {
        return customerDao.selectCustomerId();
    }

    @Override
    public Long selectOrderId() {
        return customerDao.selectOrderId();
    }

    @Override
    public void insertCustomerOrderId(QueryVo queryVo) {
        customerDao.insertCustomerOrderId(queryVo);
    }

    @Override
    public void updateCustomer(CustomerOrderDto customerOrderDto) {
        customerDao.updateCustomer(customerOrderDto);
    }

    @Override
    public void updateOrder(Order order) {
        customerDao.updateOrder(order);
    }

    @Override
    public void deleteCustomers(List<String> groupId) {
        customerDao.deleteCustomers(groupId);
    }
}
