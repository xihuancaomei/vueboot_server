package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.OnlineGameDao;
import com.asianrapid.model.sys.BookEntity;
import com.asianrapid.model.sys.OnLineGameDictEntity;
import com.asianrapid.model.sys.OnlineGameEntity;
import com.asianrapid.service.sys.OnlineGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @autohr bh
 * @description
 * @date 2020/9/7
 */
@Service
public class OnlineGameServiceImpl  implements OnlineGameService {

    @Autowired
    OnlineGameDao onlineGameDao;

    @Override
    public List<OnlineGameEntity> onlineGamesList(String gameName, String gameType, String gameOperation, int pageSize, int start) {
        return onlineGameDao.selectOnlineGamesList(gameName,gameType,gameOperation,pageSize,start);
    }

    @Override
    public Integer onlineGamesSize(String gameName, String gameType, String gameOperation, int pageSize, int start) {
        return onlineGameDao.selectOnlineGamesSize(gameName,gameType,gameOperation,pageSize,start);
    }

    @Override
    public void insertOnlineGames(OnlineGameEntity onlineGameEntity) {
        onlineGameDao.insertOnlineGames(onlineGameEntity);
    }

    @Override
    public void updateOnlineGames(OnlineGameEntity onlineGameEntity) {
        onlineGameDao.updateOnlineGames(onlineGameEntity);
    }

    @Override
    public void deleteOnlineGames(List<String> groupId) {
        onlineGameDao.deleteOnlineGames(groupId);
    }

    @Override
    public List<OnLineGameDictEntity> selectTypeList() {
        return onlineGameDao.selectTypeList();
    }

    @Override
    public List<OnLineGameDictEntity> selectOperationList() {
        return onlineGameDao.selectOperationList();
    }

//    @Override
//    public List<String[]> selectOnlineGamesList() {
//        String[] OperationList = onlineGameDao.selectOperationList();
//        String[] TypeList = onlineGameDao.selectTypeList();
//        List<String[]> list=new ArrayList<String[]>();
//        list.add(TypeList);
//        list.add(OperationList);
//        return list;
//    }
}
