package com.asianrapid.service.impl.sys;


import com.asianrapid.dao.FlowerDao;
import com.asianrapid.model.sys.FlowerEntity;
import com.asianrapid.service.sys.FlowerService;
import com.asianrapid.service.sys.ToyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("flowerServiceImpl")
public class FlowerServiceImpl implements FlowerService {
	@Autowired
	private FlowerDao flowerDao;


	@Override
	public List<FlowerEntity> flowersList(int pageSize, String flowerType, String flowerName, int start) {
		return flowerDao.flowersList(pageSize, flowerType, flowerName, start);
	}

	@Override
	public Integer flowersSize(int pageSize, String flowerType, String flowerName, int start) {
		return flowerDao.flowersSize(pageSize,flowerType,flowerName,start);
	}

	@Override
	public void insertFlower(FlowerEntity flowerEntity) {
		flowerDao.insertFlower(flowerEntity);
	}

	@Override
	public void updateFlower(FlowerEntity flowerEntity) {
		flowerDao.updateFlower(flowerEntity);
	}

	@Override
	public void deleteFlowers(List<String> groupId) {
		flowerDao.deleteFlowers(groupId);
	}

	@Override
	public List<FlowerEntity> flowerTypeList() {
		return flowerDao.flowerTypeList();
	}
}