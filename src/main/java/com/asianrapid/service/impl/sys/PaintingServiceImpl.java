package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.PaintingDao;
import com.asianrapid.model.sys.PaintingEntity;
import com.asianrapid.service.sys.PaintingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr lx
 * @description
 * @date 2020/9/7
 */
@Service(value = "PaintingServiceImpl")
public class PaintingServiceImpl implements PaintingService {

    @Autowired
    private PaintingDao paintingDao;

    @Override
    public List<PaintingEntity> paintingList(String paintingName, String paintingType, int pageSize, int start) {
        return paintingDao.paintingList(paintingName,paintingType,pageSize,start);
    }

    @Override
    public Integer paintingSize(String paintingName, String paintingType, int pageSize, int start) {
        return paintingDao.paintingSize(paintingName,paintingType,pageSize,start);
    }

    @Override
    public void insertpainting(PaintingEntity paintingEntity) {
        paintingDao.insertpainting(paintingEntity);
    }

    @Override
    public List<PaintingEntity> paintingTypeList() {
        return paintingDao.paintingTypeList();
    }

    @Override
    public void deletePainting(List<String> groupId) {
        paintingDao.deletePainting(groupId);
    }

    @Override
    public void updatePainting(PaintingEntity paintingEntity) {
        paintingDao.updatePainting(paintingEntity);
    }

    @Override
    public PaintingEntity getPaintingEntity(String paintingName) {
        return paintingDao.getPaintingEntity(paintingName);
    }
}
