package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.CartoonDao;
import com.asianrapid.model.sys.CartoonEntity;
import com.asianrapid.service.sys.CartoonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/7
 */
@Service("cartoonServiceImpl")
public class CartoonServiceImpl implements CartoonService {

    @Autowired
    private CartoonDao cartoonDao;

    @Override
    public List<CartoonEntity> cartoonsList(int pageSize, String cartoonCategory, String cartoonName, int start) {
        return cartoonDao.cartoonsList(pageSize , cartoonCategory, cartoonName, start);
    }

    @Override
    public Integer cartoonsSize(int pageSize, String cartoonCategory, String cartoonName, int start) {
        return cartoonDao.cartoonsSize(pageSize , cartoonCategory, cartoonName, start);
    }

    @Override
    public void insertCartoon(CartoonEntity cartoonEntity) {
        cartoonDao.insertCartoon(cartoonEntity);
    }

    @Override
    public void updateCartoon(CartoonEntity cartoonEntity) {
        cartoonDao.updateCartoon(cartoonEntity);
    }

    @Override
    public void deleteCartoons(List<String> groupId) {
        cartoonDao.deleteCartoons(groupId);
    }

    @Override
    public List<CartoonEntity> cartoonList() {
        return cartoonDao.cartoonList();
    }
}
