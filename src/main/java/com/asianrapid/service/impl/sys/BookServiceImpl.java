package com.asianrapid.service.impl.sys;


import com.asianrapid.dao.BookDao;
import com.asianrapid.model.sys.BookEntity;
import com.asianrapid.model.sys.UserEntity;
import com.asianrapid.service.sys.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDao bookDao;

    @Override
    public List<BookEntity> selectBookList() {
        return bookDao.selectBookList();
    }

    @Override
    public List<BookEntity> booksList(String bookName, String bookAuthor, String bookPress, int pageSize, int start) {
        return bookDao.selectBooksList(bookName, bookAuthor, bookPress, pageSize, start);
    }

    @Override
    public Integer booksSize(String bookName, String bookAuthor, String bookPress, int pageSize, int start) {
        return bookDao.selectBooksSize(bookName, bookAuthor, bookPress, pageSize, start);
    }

    @Override
    public void insertBook(BookEntity bookEntity) {
        bookDao.insertBook(bookEntity);
    }

    @Override
    public void updateBook(BookEntity bookEntity) {
        bookDao.updateBook(bookEntity);

    }

    @Override
    public void deleteBooks(List<String> groupId) {
        bookDao.deleteBooks(groupId);

    }
}
