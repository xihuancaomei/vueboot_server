package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.StudentDao;
import com.asianrapid.model.sys.StudentEntity;
import com.asianrapid.service.sys.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/7
 */
@Service(value = "studentServiceImpl")
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;

    @Override
    public List<StudentEntity> getStdList(String stdName,String stdNumber,
                                          String stdRoom,String stdClass,String stdTeacher,int pageSize, int start) {
        return studentDao.getStdList(stdName,stdNumber,stdRoom,stdClass,stdTeacher,pageSize,start);
    }

    @Override
    public Integer getStdListCount(String stdName,String stdNumber,
                                   String stdRoom,String stdClass,String stdTeacher,int pageSize, int start) {
        return studentDao.getStdListCount(stdName,stdNumber,stdRoom,stdClass,stdTeacher,pageSize,start);
    }

    @Override
    public List<String> getStdClassList() {
        return studentDao.getStdClassList();
    }

    @Override
    public List<String> getStdTeacherList() {
        return studentDao.getStdTeacherList();
    }

    @Override
    public void insertStd(StudentEntity std) {
        studentDao.insertStd(std);
    }

    @Override
    public void updateStd(StudentEntity std) {
        studentDao.updateStd(std);
    }

    @Override
    public void deleteStd(Long[] stdIds) {
        studentDao.deleteStd(stdIds);
    }
}
