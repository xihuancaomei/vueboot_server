package com.asianrapid.service.impl.sys;


import com.asianrapid.dao.ToyDao;
import com.asianrapid.model.sys.ToyEntity;
import com.asianrapid.service.sys.ToyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("toyServiceImpl")
public class ToyServiceImpl implements ToyService {
	@Autowired
	private ToyDao toyDao;

	@Override
	public List<ToyEntity> toysList(int pageSize, String name, String effect, int start) {
		return toyDao.toysList(pageSize,name,effect,start);
	}

	@Override
	public Integer toysSize(int pageSize, String name, String effect, int start) {
		return toyDao.toysSize(pageSize,name,effect,start);
	}

	@Override
	public List<ToyEntity> nameList() {
		return toyDao.nameList();
	}

	@Override
	public void insertToy(ToyEntity toyEntity) {
		toyDao.insertToy(toyEntity);
	}

	@Override
	public void updateToy(ToyEntity toyEntity) {
		toyDao.updateToy(toyEntity);
	}

	@Override
	public void deleteToys(List<String> groupId) {
		toyDao.deleteToys(groupId);
	}


}
