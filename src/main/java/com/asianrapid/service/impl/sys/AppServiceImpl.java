package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.AppDao;
import com.asianrapid.model.sys.AppEntity;
import com.asianrapid.service.sys.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/7
 */@Service
public class AppServiceImpl implements AppService {
    @Autowired
    private AppDao appDao;

    @Override
    public List<AppEntity> getAppListById(String[] ids) {
        return appDao.getAppListById(ids);
    }

    @Override
    public List<AppEntity> appsList(int pageSize, String appOnline, String appType, int start) {
        return appDao.appsList(pageSize,appOnline,appType,start);
    }

    @Override
    public Integer appsSize(int pageSize, String appOnline, String appType, int start) {
        return appDao.appsSize(pageSize,appOnline,appType,start);
    }

    @Override
    public void insertApp(AppEntity appEntity) {
        appDao.insertApp(appEntity);
    }

    @Override
    public void updateApp(AppEntity appEntity) {
        appDao.updateApp(appEntity);
    }

    @Override
    public void deleteApps(List<String> groupId) {
        appDao.deleteApps(groupId);
    }

    @Override
    public List<AppEntity> appsByLine() {
        return appDao.appsByLine();
    }
}
