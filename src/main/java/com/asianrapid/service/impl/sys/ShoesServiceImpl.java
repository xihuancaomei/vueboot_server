package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.ShoesDao;
import com.asianrapid.model.sys.ShoesEntity;
import com.asianrapid.service.sys.ShoesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gx
 * @description
 * @date 2020/9/7
 */
@Service(value = "shoesServiceImpl")
public class ShoesServiceImpl implements ShoesService {

    @Autowired
    private ShoesDao shoesDao;

    @Override
    public List<ShoesEntity> shoesList(int pageSize, int start, String type, String brand) {
        return shoesDao.shoesList(pageSize,start,type,brand);
    }

    @Override
    public Integer shoesSize(int pageSize, int start, String type, String brand) {
        return shoesDao.shoesSize(pageSize,start,type,brand);
    }

    @Override
    public void insertShoes(ShoesEntity shoesEntity) {
        shoesDao.insertShoes(shoesEntity);
    }

    @Override
    public List<ShoesEntity> allShoes() {
        return shoesDao.allShoes();
    }

    @Override
    public void updateShoes(ShoesEntity shoesEntity) {
        shoesDao.updateShoes(shoesEntity);
    }

    @Override
    public void deleteShoes(List<String> groupId) {
        shoesDao.deleteShoes(groupId);
    }

    @Override
    public List<ShoesEntity> shoesByType() {
        return shoesDao.shoesByType();
    }
}
