package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.ApDao;

import com.asianrapid.model.sys.ApEntity;
import com.asianrapid.service.sys.ApService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/9
 */@Service( "apServiceImpl")
public class ApServiceImpl implements ApService {
    @Autowired
    private ApDao apDao;

    @Override
    public List<ApEntity> getApListById(String[] ids) {
        return apDao.getApListById(ids);
    }

    @Override
    public List<ApEntity> apsList(int pageSize, String apOnline, String apType, int start) {
        return apDao.apsList(pageSize,apOnline,apType,start);
    }

    @Override
    public Integer apsSize(int pageSize, String apOnline, String apType, int start) {
        return apDao.apsSize(pageSize,apOnline,apType,start);
    }

    @Override
    public void insertAp(ApEntity apEntity) {
        apDao.insertAp(apEntity);
    }

    @Override
    public void updateAp(ApEntity apEntity) {
        apDao.updateAp(apEntity);
    }

    @Override
    public void deleteAps(List<String> groupId) {
        apDao.deleteAps(groupId);
    }

    @Override
    public List<ApEntity> apsByLine() {
        return apDao.apsByLine();
    }
}
