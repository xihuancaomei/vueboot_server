package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.ARCarDao;
import com.asianrapid.model.sys.ARCarEntity;
import com.asianrapid.service.sys.ARCarService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/9
 */
@Service(value = "arCarServiceImpl")
public class ARCarServiceImpl implements ARCarService {

    @Autowired
    private ARCarDao arCarDao;

    @Override
    public List<ARCarEntity> getCarList(int pageSize, int start, @Param("carType") String carType, @Param("carTrans") String carTrans,
                                        @Param("carOrign") String carOrign) {
        return arCarDao.getCarList(pageSize,start,carType,carTrans,carOrign);
    }

    @Override
    public Integer getCarListCount(@Param("carType") String carType,@Param("carTrans") String carTrans,
                                   @Param("carOrign") String carOrign) {
        return arCarDao.getCarListCount(carType,carTrans,carOrign);
    }

    @Override
    public List<String> getCarTransList() {
        return arCarDao.getCarTransList();
    }

    @Override
    public List<String> getCarOrignList() {
        return arCarDao.getCarOrignList();
    }

    @Override
    public void insertCar(ARCarEntity arCar) {
        arCarDao.insertCar(arCar);
    }

    @Override
    public void updateCar(ARCarEntity arCar) {
        arCarDao.updateCar(arCar);
    }

    @Override
    public void deleteCars(Long[] carIds) {
        arCarDao.deleteCars(carIds);
    }
}
