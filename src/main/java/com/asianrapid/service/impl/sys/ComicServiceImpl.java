package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.ComicDao;
import com.asianrapid.model.sys.ComicEntity;
import com.asianrapid.service.sys.ComicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/8
 */
@Service("comicServiceImpl")
public class ComicServiceImpl implements ComicService {

    @Autowired
    private ComicDao comicDao;
    @Override
    public List<ComicEntity> comicsList(int pageSize, String comicCategory, String comicName, int start) {
        return comicDao.comicsList(pageSize , comicCategory, comicName, start);
    }

    @Override
    public Integer comicsSize(int pageSize, String comicCategory, String comicName, int start) {
        return comicDao.comicsSize(pageSize , comicCategory, comicName, start);
    }

    @Override
    public void insertComic(ComicEntity comicEntity) {
        comicDao.insertComic(comicEntity);
    }

    @Override
    public void updateComic(ComicEntity comicEntity) {
        comicDao.updateComic(comicEntity);
    }

    @Override
    public void deleteComics(List<String> groupId) {
        comicDao.deleteComics(groupId);
    }

    @Override
    public List<ComicEntity> comicList() {
        return comicDao.comicList();
    }
}
