package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.SmokeDao;
import com.asianrapid.model.sys.SmokeEntity;
import com.asianrapid.model.sys.UserEntity;
import com.asianrapid.service.sys.SmokeService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @autohr lzd
 * @description
 * @date 2020/9/3
 */
@Service
public class SmokeServiceImpl implements SmokeService {
    @Autowired
    private SmokeDao smokeDao;
    public ArrayList<SmokeEntity> getSmokeList(String smokeTar, String smokeType, String smokeName, int pageSize, int start) {
        return smokeDao.getSmokeList(smokeTar,smokeType,smokeName,pageSize,start);
    }
    @Override
    public Integer getSmokeListSize(String smokeTar,String smokeType,String smokeName ,int pageSize, int start) {
        return smokeDao.getSmokeListSize(smokeTar,smokeType,smokeName,pageSize, start);
    }
    @Override
    public void insertSmoke(SmokeEntity smokeEntity) {
        smokeDao.insertSmoke(smokeEntity);
    }
    @Override
    public void updateSmokes(SmokeEntity smokeEntity) {
        smokeDao.updateSmokes(smokeEntity);
    }
    @Override
    public void deleteSmoke(List<String> groupId) {
        smokeDao.deleteSmoke(groupId);
    }
    @Override
    public List<SmokeEntity> getSmokeTypeList() {
        return smokeDao.getSmokeTypeList();
    }

}
