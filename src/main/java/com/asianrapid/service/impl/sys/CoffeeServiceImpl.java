package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.CoffeeDao;
import com.asianrapid.model.sys.CoffeeEntity;
import com.asianrapid.service.sys.CoffeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/3
 */
@Service("coffeeServiceImpl")
public class CoffeeServiceImpl implements CoffeeService {

    @Autowired
    private CoffeeDao coffeeDao;

    @Override
    public List<CoffeeEntity> getCoffeeListById(String[] ids) {
        return coffeeDao.getCoffeeListById(ids);
    }

    @Override
    public List<CoffeeEntity> coffeesList(int pageSize,String coffeeName, String coffeeAddSugar, int start) {
        return coffeeDao.coffeesList(pageSize,coffeeName,coffeeAddSugar,start);
    }

    @Override
    public Integer coffeesSize(int pageSize, String coffeeName, String coffeeAddSugar,int start) {
        return coffeeDao.coffeesSize(pageSize,coffeeName,coffeeAddSugar,start);
    }

    @Override
    public void insertCoffee(CoffeeEntity coffeeEntity) {
        coffeeDao.insertCoffee(coffeeEntity);
    }

    @Override
    public void updateCoffee(CoffeeEntity coffeeEntity) {
        coffeeDao.updateCoffee(coffeeEntity);
    }

    @Override
    public void deleteCoffees(List<String> groupId) {
        coffeeDao.deleteCoffees(groupId);
    }

    @Override
    public List<CoffeeEntity> coffeesByName() {
        return coffeeDao.coffeesByName();
    }
}
