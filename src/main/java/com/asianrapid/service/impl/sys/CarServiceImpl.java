package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.CarDao;
import com.asianrapid.model.sys.CarEntity;
import com.asianrapid.service.sys.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/3
 */
@Service(value = "carServiceImpl")
public class CarServiceImpl implements CarService {
    @Autowired
    private CarDao carDao;

    @Override
    public List<CarEntity> getCarList(String carType,String carTransmission,String carOrign,int pageSize, int start) {
        return carDao.getCarList(carType,carTransmission,carOrign,pageSize, start);
    }

    @Override
    public Integer getCarListSize(String carType,String carTransmission,String carOrign,int pageSize, int start) {
        return carDao.getCarListSize(carType,carTransmission,carOrign,pageSize,start);
    }

    @Override
    public void insertCar(CarEntity carEntity) {
        carDao.insertCar(carEntity);
    }

    @Override
    public void updateCar(CarEntity carEntity) {
        carDao.updateCar(carEntity);
    }

    @Override
    public void deleteCar(Long[] carIds) {
        carDao.deleteCar(carIds);
    }

    @Override
    public List<CarEntity> getCarTypeList() {
        return carDao.getCarTypeList();
    }

}
