package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.MouseDao;
import com.asianrapid.model.sys.MouseEntity;
import com.asianrapid.service.sys.MouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "mouseServiceImpl")
public class MouseServiceImpl implements MouseService {

    @Autowired
    private MouseDao mouseDao;

    @Override
    public List<MouseEntity> mousesList(String mouseName, String mouseBrand,  String mouseCat, int pageSize, int start) {
        return mouseDao.mousesList(mouseName, mouseBrand, mouseCat, pageSize, start);
    }

    @Override
    public Integer mousesSize(String mouseName, String mouseBrand, String mouseCat, int pageSize, int start) {
        return mouseDao.mousesSize(mouseName, mouseBrand, mouseCat, pageSize, start);
    }

    @Override
    public List<MouseEntity> allMouses() {
        return mouseDao.allMouses();
    }

    @Override
    public void insertMouse(MouseEntity mouseEntity) {
        mouseDao.insertMouse(mouseEntity);
    }

    @Override
    public void updateMouse(MouseEntity mouseEntity) {
        mouseDao.updateMouse(mouseEntity);
    }

    @Override
    public void deleteMouses(List<String> groupId) {
        mouseDao.deleteMouses(groupId);
    }

}
