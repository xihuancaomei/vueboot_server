package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.KeyboardDao;
import com.asianrapid.model.sys.Keyboard;
import com.asianrapid.service.sys.KeyboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class KeyboardServiceImpl implements KeyboardService {

    @Autowired
    private KeyboardDao keyboardDao;

    @Override
    public String[] selectBrandList() {
        return keyboardDao.selectBrandList();
    }

    @Override
    public String[] selectCatList() {
        return keyboardDao.selectCatList();
    }

    @Override
    public List<Keyboard> keyboardsList(String keyboardName, String keyboardBrand, String keyboardCat, int pageSize, int start) {
        return keyboardDao.keyboardsList(keyboardName, keyboardBrand, keyboardCat, pageSize, start);
    }

    @Override
    public Integer keyboardSize(String keyboardName, String keyboardBrand, String keyboardCat, int pageSize, int start) {
        return keyboardDao.keyboardSize(keyboardName, keyboardBrand, keyboardCat, pageSize, start);
    }

    @Override
    public void insertKeyboard(Keyboard keyboard) {
        keyboardDao.insertKeyboard(keyboard);
    }

    @Override
    public void updateKeyboard(Keyboard keyboard) {
        keyboardDao.updateKeyboard(keyboard);
    }

    @Override
    public void deleteKeyboards(List<String> groupId) {
        keyboardDao.deleteKeyboards(groupId);
    }
}
