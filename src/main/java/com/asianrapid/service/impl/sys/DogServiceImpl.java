package com.asianrapid.service.impl.sys;

import java.util.List;
import com.asianrapid.dao.DogDao;
import com.asianrapid.model.sys.DogEntity;
import com.asianrapid.service.sys.DogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/4
 */
@Service("dogServiceImpl")
public class DogServiceImpl implements DogService {
    @Autowired
    private DogDao dogDao;

    @Override
    public List<DogEntity> dogsList(int pageSize, String fur, String name, int start) {
        return dogDao.dogsList(pageSize , fur, name, start);
    }

    @Override
    public Integer dogsSize(int pageSize, String fur, String name, int start) {
        return dogDao.dogsSize(pageSize, fur, name, start);
    }

    @Override
    public void insertDog(DogEntity dogEntity) {
        dogDao.insertDog(dogEntity);
    }

    @Override
    public void updateDog(DogEntity dogEntity) {
        dogDao.updateDog(dogEntity);
    }

    @Override
    public void deleteDogs(List<String> groupId) {
        dogDao.deleteDogs(groupId);
    }

    @Override
    public List<DogEntity> dogList() {
        return dogDao.dogList();
    }


}
