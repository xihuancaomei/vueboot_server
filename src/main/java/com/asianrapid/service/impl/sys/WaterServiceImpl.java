package com.asianrapid.service.impl.sys;

import com.asianrapid.dao.WaterDao;
import com.asianrapid.model.sys.WaterEntity;
import com.asianrapid.service.sys.WaterService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autohr lzd
 * @description
 * @date 2020/9/7
 */
@Service
public class WaterServiceImpl implements WaterService {
    @Autowired
    private WaterDao waterDao;
    @Override
    public List<WaterEntity> getWatersList(String waterCapacity,String waterType,String waterName ,int pageSize, int start) {
        return waterDao.getWatersList(waterCapacity,waterType,waterName, pageSize, start);
    }

    @Override
    public Integer getWatersSize(String waterCapacity,String waterType, String waterName, int pageSize, int start) {
        return waterDao.getWatersSize(waterCapacity,waterType,waterName,pageSize, start);
    }

    @Override
    public List<WaterEntity> getWatersType() {
        return waterDao.getWatersType();
    }

    @Override
    public List<WaterEntity> getWatersCapacity() {
        return waterDao.getWatersCapacity();
    }

    @Override
    public void insertWater(WaterEntity waterEntity) {
        waterDao.insertWater(waterEntity);
    }

    @Override
    public void deleteWaterById(List<String> groupId) {
        waterDao.deleteWaterById(groupId);
    }

    @Override
    public void updateWaterById(WaterEntity waterEntity) {
        waterDao.updateWaterById(waterEntity);
    }
}
