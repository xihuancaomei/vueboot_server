package com.asianrapid.service.sys;





import com.asianrapid.model.sys.PlantEntity;

import java.util.List;

public interface PlantService {

	public List<PlantEntity> plantsList(int pageSize, String plantType, String plantName, int start);
	public Integer plantsSize(int pageSize, String plantType, String plantName, int start);
	public void insertPlant(PlantEntity plantEntity);
	public void updatePlant(PlantEntity plantEntity);
	public void deletePlants(List<String> groupId);
	List<PlantEntity> plantTypeList();
}