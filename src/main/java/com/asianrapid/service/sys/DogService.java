package com.asianrapid.service.sys;

import com.asianrapid.model.sys.DogEntity;

import java.util.List;

public interface DogService {
    List<DogEntity> dogsList(int pageSize, String fur, String name, int start);
    Integer dogsSize(int pageSize, String fur, String name, int start);
    void insertDog(DogEntity dogEntity);
    void updateDog(DogEntity dogEntity);
    void deleteDogs(List<String> groupId);
    public List<DogEntity> dogList();
}
