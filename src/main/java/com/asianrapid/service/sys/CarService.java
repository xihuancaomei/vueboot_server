package com.asianrapid.service.sys;

import com.asianrapid.model.sys.CarEntity;
import com.asianrapid.model.sys.RoleEntity;

import java.util.List;

public interface CarService {
    List<CarEntity> getCarList(String carType,String carTransmission,String carOrign,int pageSize, int start);
    Integer getCarListSize(String carType,String carTransmission,String carOrign,int pageSize, int start);
    void insertCar(CarEntity carEntity);
    void updateCar(CarEntity carEntity);
    void deleteCar(Long[] carIds);
    List<CarEntity> getCarTypeList();
}
