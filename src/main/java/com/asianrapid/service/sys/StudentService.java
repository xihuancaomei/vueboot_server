package com.asianrapid.service.sys;

import com.asianrapid.model.sys.StudentEntity;

import java.util.List;

public interface StudentService {
    List<StudentEntity> getStdList(String stdName,String stdNumber,String stdRoom,
                                   String stdClass,String stdTeacher,int pageSize, int start);
    Integer getStdListCount(String stdName,String stdNumber,String stdRoom,
                            String stdClass,String stdTeacher,int pageSize, int start);
    List<String> getStdClassList();
    List<String> getStdTeacherList();
    void insertStd(StudentEntity std);
    void updateStd(StudentEntity std);
    void deleteStd(Long[] stdIds);
}
