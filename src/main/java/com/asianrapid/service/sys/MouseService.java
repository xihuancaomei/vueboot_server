package com.asianrapid.service.sys;

import com.asianrapid.model.sys.MouseEntity;

import java.util.List;

public interface MouseService {

    public List<MouseEntity> mousesList(String mouseName, String mouseBrand, String mouseCat, int pageSize, int start);

    public Integer mousesSize(String mouseName, String mouseBrand, String mouseCat, int pageSize, int start);

    public List<MouseEntity> allMouses();

    public void insertMouse(MouseEntity mouseEntity);

    public void updateMouse(MouseEntity mouseEntity);

    public void deleteMouses(List<String> groupId);

}
