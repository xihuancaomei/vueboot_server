package com.asianrapid.service.sys;

import com.asianrapid.model.sys.AppEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/7
 */
public interface AppService {
    public List<AppEntity> getAppListById(@Param("ids") String[] ids);


    public List<AppEntity> appsList(@Param("pageSize") int pageSize, @Param("appOnline") String appOnline,@Param("appType") String appType, @Param("start") int start);


    public Integer appsSize(@Param("pageSize") int pageSize,@Param("appOnline") String appOnline, @Param("appType") String appType,@Param("start") int start);


    public void insertApp(@Param("appEntity") AppEntity appEntity);


    public void updateApp(@Param("appEntity") AppEntity appEntity);


    public void deleteApps(@Param("groupId") List<String> groupId);

    public List<AppEntity> appsByLine();
}
