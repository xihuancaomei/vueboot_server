package com.asianrapid.service.sys;

import com.asianrapid.model.sys.SmokeEntity;
import com.asianrapid.model.sys.UserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface SmokeService {
    public ArrayList<SmokeEntity> getSmokeList(String smokeTar, String smokeType, String smokeName, @Param("pageSize") int pageSize, @Param("start") int start);
    public Integer getSmokeListSize(@Param("smokeTar") String smokeTar,@Param("smokeType") String smokeType, @Param("smokeName") String smokeName, @Param("pageSize") int pageSize, @Param("start") int start);
    public void insertSmoke(SmokeEntity smokeEntity);
    public void updateSmokes(SmokeEntity smokeEntity);
    public void deleteSmoke(List<String> groupId);
    public List<SmokeEntity> getSmokeTypeList();

}
