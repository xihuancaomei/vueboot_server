package com.asianrapid.service.sys;

import com.asianrapid.model.sys.ComicEntity;

import java.util.List;

public interface ComicService {
    List<ComicEntity> comicsList(int pageSize, String comicCategory, String comicName, int start);
    Integer comicsSize(int pageSize, String comicCategory, String comicName, int start);
    void insertComic(ComicEntity comicEntity);
    void updateComic(ComicEntity comicEntity);
    void deleteComics(List<String> groupId);
    public List<ComicEntity> comicList();
}
