package com.asianrapid.service.sys;

import com.asianrapid.model.sys.BookEntity;
import com.asianrapid.model.sys.OnLineGameDictEntity;
import com.asianrapid.model.sys.OnlineGameEntity;

import java.util.List;

public interface OnlineGameService {

    /**
     * 获取online_game表
     */
    public List<OnlineGameEntity> onlineGamesList(String gameName, String gameType, String gameOperation, int pageSize, int start);

    /**
     * 获取online_game表的总数
     */
    public Integer onlineGamesSize(String gameName, String gameType, String gameOperation, int pageSize, int start);

    /**
     * 新增online_game数据
     */
    public void insertOnlineGames(OnlineGameEntity onlineGameEntity);

    /**
     * 更新online_game信息
     */
    public void updateOnlineGames(OnlineGameEntity onlineGameEntity);

    /**
     * 删除online_game信息
     */
    public void deleteOnlineGames(List<String> groupId);

    /**
     * 获取全部下拉
     */
    public List<OnLineGameDictEntity> selectTypeList();

    public List<OnLineGameDictEntity> selectOperationList();
}
