package com.asianrapid.service.sys;

import com.asianrapid.model.sys.PaintingEntity;

import java.util.List;

public interface PaintingService {
    /**
     * 获取role列表
     *
     * @param pageSize
     * @param page
     * @return
     */
    public List<PaintingEntity> paintingList(String paintingName, String paintingType, int pageSize, int start);

    /**
     * 获取画作列表的总量
     *
     * @param pageSize
     * @param page
     * @return
     */
    public Integer paintingSize(String paintingName,String paintingType,int pageSize, int start);
    /**
     * 新建画作信息
     *
     * @param paintingEntity
     */
    public void insertpainting(PaintingEntity paintingEntity);
    /**
     * 获取画作表下拉框
     */
    public List<PaintingEntity> paintingTypeList();
    /**
     * 删除画作信息
     *
     * @param groupId
     */
    public void deletePainting(List<String> groupId);
    /**
     * 更新画作信息
     *
     * @param paintingEntity
     */
    public void updatePainting(PaintingEntity paintingEntity);
    /**
     * 通过画作名得到画作信息
     * @param paintingName
     * @return
     */
    public PaintingEntity getPaintingEntity(String paintingName);
}
