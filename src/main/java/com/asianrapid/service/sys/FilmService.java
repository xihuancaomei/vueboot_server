package com.asianrapid.service.sys;

import com.asianrapid.model.sys.Film;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FilmService {

    /**
     * 获取电影列表
     */
    public List<Film> filmList(int pageSize,int start,String type,String name);
    /**
     * 获取列表总量
     */
    public Integer filmSize(int pageSize, int start,String type,String name);
    /**
     * 新增电影
     */
    public void insertFilm(Film filmEntity);
    /**
     * 返显
     */
    public List<Film> allFilm();
    /**
     * 修改电影
     */
    public void updateFilm(Film filmEntity);
    /**
     * 删除电影
     */
    public void deleteFilm(List<String> groupId);

    public List<Film> filmByType();
}
