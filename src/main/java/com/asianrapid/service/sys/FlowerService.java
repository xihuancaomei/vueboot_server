package com.asianrapid.service.sys;




import com.asianrapid.model.sys.FlowerEntity;

import java.util.List;

public interface FlowerService {

	public List<FlowerEntity> flowersList(int pageSize, String flowerType, String flowerName, int start);
	public Integer flowersSize(int pageSize, String flowerType, String flowerName, int start);
	public void insertFlower(FlowerEntity flowerEntity);
	public void updateFlower(FlowerEntity flowerEntity);
	public void deleteFlowers(List<String> groupId);
	List<FlowerEntity> flowerTypeList();
}