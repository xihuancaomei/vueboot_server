package com.asianrapid.service.sys;

import com.asianrapid.model.sys.ARCarEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ARCarService {
    List<ARCarEntity> getCarList(@Param("pageSize") int pageSize, @Param("start") int start,
                                 @Param("carType") String carType,@Param("carTrans") String carTrans,
                                 @Param("carOrign") String carOrign);
    Integer getCarListCount(@Param("carType") String carType,@Param("carTrans") String carTrans,
                            @Param("carOrign") String carOrign);
    List<String> getCarTransList();
    List<String> getCarOrignList();
    void insertCar(ARCarEntity arCar);
    void updateCar(ARCarEntity arCar);
    void deleteCars(Long[] carIds);
}
