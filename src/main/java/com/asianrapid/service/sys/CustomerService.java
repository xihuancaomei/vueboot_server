package com.asianrapid.service.sys;

import com.asianrapid.model.sys.Customer;
import com.asianrapid.model.sys.CustomerOrderDto;
import com.asianrapid.model.sys.Order;
import com.asianrapid.model.sys.QueryVo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface CustomerService {

    public List<String> selectOrderNameList();

    public List<CustomerOrderDto> customersList(String customerName, String orderId, String name, int pageSize, int start);

    public Integer customerSize(String customerName, String orderId, String name, int pageSize, int start);

    public void insertCustomer(CustomerOrderDto customerOrderDto);

    public void insertOrder(CustomerOrderDto customerOrderDto);

    public Long selectCustomerId();

    public Long selectOrderId();

    public void insertCustomerOrderId(QueryVo queryVo);

    public void updateCustomer(CustomerOrderDto customerOrderDto);

    public void updateOrder(Order order);

    public void deleteCustomers(List<String> groupId);

}
