package com.asianrapid.service.sys;

import com.asianrapid.model.sys.Keyboard;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface KeyboardService {

    public String[] selectBrandList();

    public String[] selectCatList();

    public List<Keyboard> keyboardsList(String keyboardName, String keyboardBrand, String keyboardCat, int pageSize, int start);

    public Integer keyboardSize(String keyboardName, String keyboardBrand, String keyboardCat, int pageSize, int start);

    public void insertKeyboard(Keyboard keyboard);

    public void updateKeyboard(Keyboard keyboard);

    public void deleteKeyboards(List<String> groupId);
}
