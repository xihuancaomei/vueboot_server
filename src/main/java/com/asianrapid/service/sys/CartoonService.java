package com.asianrapid.service.sys;

import com.asianrapid.model.sys.CartoonEntity;

import java.util.List;

public interface CartoonService {
    List<CartoonEntity> cartoonsList(int pageSize, String cartoonCategory, String cartoonName, int start);
    Integer cartoonsSize(int pageSize, String cartoonCategory, String cartoonName, int start);
    void insertCartoon(CartoonEntity cartoonEntity);
    void updateCartoon(CartoonEntity cartoonEntity);
    void deleteCartoons(List<String> groupId);
    public List<CartoonEntity> cartoonList();
}
