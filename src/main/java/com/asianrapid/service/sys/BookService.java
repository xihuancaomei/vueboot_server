package com.asianrapid.service.sys;

import com.asianrapid.model.sys.BookEntity;
import com.asianrapid.model.sys.UserEntity;

import java.util.List;

public interface BookService {


    /**
     * 获取全部
     */
    public List<BookEntity> selectBookList();
    /**
     * 获取book表
     */
	public List<BookEntity> booksList(String bookName, String bookAuthor, String bookPress, int pageSize, int page);

    /**
     * 获取book表的总数

     */
	public Integer booksSize(String bookName, String bookAuthor, String bookPress, int pageSize, int page);

	/**
	 * 新建book信息
	 */
	public void insertBook(BookEntity bookEntity);

	/**
	 * 更新book信息
	 */
	public void updateBook(BookEntity bookEntity);

	/**
	 * 删除book信息
	 */
	public void deleteBooks(List<String> groupId);
}
