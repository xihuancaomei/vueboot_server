package com.asianrapid.service.sys;

import com.asianrapid.model.sys.GameEntity;

import java.util.List;

public interface GameService {
    /**
     * 获取role列表
     *
     * @param pageSize
     * @param page
     * @return
     */
    public List<GameEntity> gamesList(String gameName,String gameType,int pageSize, int start);

    /**
     * 获取role列表的总量
     *
     * @param pageSize
     * @param page
     * @return
     */
    public Integer gamesSize(String gameName,String gameType,int pageSize, int start);

    /**
     * 新建角色信息
     *
     * @param gameEntity
     */
    public void insertGame(GameEntity gameEntity);

    /**
     * 更新角色信息
     *
     * @param gameEntity
     */
    public void updateGame(GameEntity gameEntity);

    /**
     * 删除角色信息
     *
     * @param groupId
     */
    public void deleteGames(List<String> groupId);

    /**
     * 得到角色全部数据
     * @return
     */
    public List<GameEntity> allGames();
    /**
     * 通过登录名得到用户信息
     * @param gameName
     * @return
     */
    public GameEntity getGameEntityByGameName(String gameName);

    public List<GameEntity> gameTypeList();
}
