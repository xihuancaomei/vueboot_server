package com.asianrapid.service.sys;

import com.asianrapid.model.sys.ApEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/9
 */
public interface ApService {
    public List<ApEntity> getApListById(@Param("ids") String[] ids);


    public List<ApEntity> apsList(@Param("pageSize") int pageSize, @Param("apOnline") String apOnline,@Param("apType") String apType, @Param("start") int start);


    public Integer apsSize(@Param("pageSize") int pageSize,@Param("apOnline") String apOnline, @Param("apType") String apType,@Param("start") int start);


    public void insertAp(@Param("apEntity") ApEntity apEntity);


    public void updateAp(@Param("apEntity") ApEntity apEntity);


    public void deleteAps(@Param("groupId") List<String> groupId);

    public List<ApEntity> apsByLine();
}
