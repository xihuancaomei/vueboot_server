package com.asianrapid.service.sys;

import com.asianrapid.model.sys.ShoesEntity;

import java.util.List;

public interface ShoesService {

    /**
     * 获取电影列表
     */
    public List<ShoesEntity> shoesList(int pageSize, int start, String type, String brand);
    /**
     * 获取列表总量
     */
    public Integer shoesSize(int pageSize, int start,String type,String brand);
    /**
     * 新增电影
     */
    public void insertShoes(ShoesEntity shoesEntity);
    /**
     * 返显
     */
    public List<ShoesEntity> allShoes();
    /**
     * 修改电影
     */
    public void updateShoes(ShoesEntity shoesEntity);
    /**
     * 删除电影
     */
    public void deleteShoes(List<String> groupId);

    public List<ShoesEntity> shoesByType();
}
