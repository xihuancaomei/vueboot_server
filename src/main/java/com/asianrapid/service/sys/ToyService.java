package com.asianrapid.service.sys;


import com.asianrapid.model.sys.MenuEntity;
import com.asianrapid.model.sys.ToyEntity;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface ToyService {

	List<ToyEntity> toysList(int pageSize, String name, String effect, int start);
	Integer toysSize(int pageSize, String name, String effect, int start);
	void insertToy(ToyEntity toyEntity);
	void updateToy(ToyEntity toyEntity);
	void deleteToys(List<String> groupId);

/*	List<ToyEntity> toysByPopular(int popular);*/
	List<ToyEntity> nameList();
}
