package com.asianrapid.model.sys;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @autohr lzd
 * @description
 * @date 2020/9/2
 */
public class SmokeEntity {
    /*香烟ID*/
    private Integer smokeId;
    /*香烟名称*/
    private  String smokeName;
    /*香烟类型*/
    private String smokeType;
    /*香烟价格*/
    private BigDecimal smokePice;
    /*香烟生产地*/
    private String smokeAddress;
    /*焦油含量*/
    private String smokeTar;
    /*bad*/
    private String smokeDad;

    public Integer getSmokeId() {
        return smokeId;
    }

    public void setSmokeId(Integer smokeId) {
        this.smokeId = smokeId;
    }

    public String getSmokeName() {
        return smokeName;
    }

    public void setSmokeName(String smokeName) {
        this.smokeName = smokeName;
    }

    public String getSmokeType() {
        return smokeType;
    }

    public void setSmokeType(String smokeType) {
        this.smokeType = smokeType;
    }

    public BigDecimal getSmokePice() {
        return smokePice;
    }

    public void setSmokePice(BigDecimal smokePice) {
        this.smokePice = smokePice;
    }

    public String getSmokeAddress() {
        return smokeAddress;
    }

    public void setSmokeAddress(String smokeAddress) {
        this.smokeAddress = smokeAddress;
    }

    public String getSmokeTar() {
        return smokeTar;
    }

    public void setSmokeTar(String smokeTar) {
        this.smokeTar = smokeTar;
    }

    public String getSmokeDad() {
        return smokeDad;
    }

    public void setSmokeDad(String smokeDad) {
        this.smokeDad = smokeDad;
    }
    @Override
    public String toString() {
        return "SomkeEntity{" +
                "smokeId=" + smokeId +
                ", smokeName='" + smokeName + '\'' +
                ", smokeType='" + smokeType + '\'' +
                ", smokePice=" + smokePice +
                ", smokeAddress='" + smokeAddress + '\'' +
                ", smokeTar='" + smokeTar + '\'' +
                ", smokeDad='" + smokeDad + '\'' +
                '}';
    }
}
