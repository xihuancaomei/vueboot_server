package com.asianrapid.model.sys;

import java.util.List;

public class DogEntity {
    /**
     * id
     */
    private int id;
    /**
     * 菜单名
     */
    private String name;
    /**
     * fur
     */
    private String fur;
    /**
     * 价格
     */
    private int price;
    /**
     * 排序
     */
    private int sort;
    /**
     * 描述
     */
    private String remark;
    /**
     * 图标
     */
    private String icon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFur() {
        return fur;
    }

    public void setFur(String fur) {
        this.fur = fur;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "DogEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", fur='" + fur + '\'' +
                ", price=" + price +
                ", sort=" + sort +
                ", remark='" + remark + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }
}
