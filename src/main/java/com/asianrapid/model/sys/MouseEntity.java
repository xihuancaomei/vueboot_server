package com.asianrapid.model.sys;

public class MouseEntity {

    private Long mouseId;

    private String mouseName;

    private String mouseBrand;

    private String mouseCat;

    private Integer mouseNum;

    public Long getMouseId() {
        return mouseId;
    }

    public void setMouseId(Long mouseId) {
        this.mouseId = mouseId;
    }

    public String getMouseName() {
        return mouseName;
    }

    public void setMouseName(String mouseName) {
        this.mouseName = mouseName;
    }

    public String getMouseBrand() {
        return mouseBrand;
    }

    public void setMouseBrand(String mouseBrand) {
        this.mouseBrand = mouseBrand;
    }

    public String getMouseCat() {
        return mouseCat;
    }

    public void setMouseCat(String mouseCat) {
        this.mouseCat = mouseCat;
    }

    public Integer getMouseNum() {
        return mouseNum;
    }

    public void setMouseNum(Integer mouseNum) {
        this.mouseNum = mouseNum;
    }

    @Override
    public String toString() {
        return "MouseEntity{" +
                "mouseId=" + mouseId +
                ", mouseName='" + mouseName + '\'' +
                ", mouseBrand='" + mouseBrand + '\'' +
                ", mouseCat='" + mouseCat + '\'' +
                ", mouseNum=" + mouseNum +
                '}';
    }
}
