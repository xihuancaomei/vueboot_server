package com.asianrapid.model.sys;

import java.math.BigDecimal;

public class Order {

    private Long orderId;

    private Long customerId;

    private String orderName;

    private Integer orderNumber;

    private BigDecimal orderUnitPrice;

    private BigDecimal orderTotalPrice;

    public Order() {
    }

    public Order(Long orderId, Long customerId, String orderName, Integer orderNumber, BigDecimal orderUnitPrice, BigDecimal orderTotalPrice) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.orderName = orderName;
        this.orderNumber = orderNumber;
        this.orderUnitPrice = orderUnitPrice;
        this.orderTotalPrice = orderTotalPrice;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getOrderUnitPrice() {
        return orderUnitPrice;
    }

    public void setOrderUnitPrice(BigDecimal orderUnitPrice) {
        this.orderUnitPrice = orderUnitPrice;
    }

    public BigDecimal getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(BigDecimal orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", customerId=" + customerId +
                ", orderName='" + orderName + '\'' +
                ", orderNumber=" + orderNumber +
                ", orderUnitPrice=" + orderUnitPrice +
                ", orderTotalPrice=" + orderTotalPrice +
                '}';
    }
}
