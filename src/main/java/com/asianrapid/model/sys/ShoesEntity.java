package com.asianrapid.model.sys;

/**
 * @autohr gx
 * @description
 * @date 2020/9/7
 */
public class ShoesEntity {

    private Long id;
    private String brand;
    private String name;
    private Integer size;
    private String price;
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ShoesEntity{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", price='" + price + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
