package com.asianrapid.model.sys;

/**
 * @autohr lx
 * @description
 * @date 2020/9/9
 */
public class PaintingTypeEntity {
    /**
     * 画作类型id
     */
    private int paintingTypeId;
    /**
     * 画作类型名
     */
    private String paintingTypeName;

    public int getPaintingTypeId() {
        return paintingTypeId;
    }

    public void setPaintingTypeId(int paintingTypeId) {
        this.paintingTypeId = paintingTypeId;
    }

    public String getPaintingTypeName() {
        return paintingTypeName;
    }

    public void setPaintingTypeName(String paintingTypeName) {
        this.paintingTypeName = paintingTypeName;
    }

    @Override
    public String toString() {
        return "PaintingTypeEntity{" +
                "paintingTypeId=" + paintingTypeId +
                ", paintingTypeName='" + paintingTypeName + '\'' +
                '}';
    }
}
