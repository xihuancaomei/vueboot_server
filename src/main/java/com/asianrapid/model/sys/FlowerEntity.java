package com.asianrapid.model.sys;

public class FlowerEntity {
	private int flowerId;
	private String flowerName;
	private String flowerType;
	private String flowerAlias;
	private String flowerMoral;
	private String flowerIntroduction;

	public int getFlowerId() {
		return flowerId;
	}

	public void setFlowerId(int flowerId) {
		this.flowerId = flowerId;
	}

	public String getFlowerName() {
		return flowerName;
	}

	public void setFlowerName(String flowerName) {
		this.flowerName = flowerName;
	}

	public String getFlowerType() {
		return flowerType;
	}

	public void setFlowerType(String flowerType) {
		this.flowerType = flowerType;
	}

	public String getFlowerAlias() {
		return flowerAlias;
	}

	public void setFlowerAlias(String flowerAlias) {
		this.flowerAlias = flowerAlias;
	}

	public String getFlowerMoral() {
		return flowerMoral;
	}

	public void setFlowerMoral(String flowerMoral) {
		this.flowerMoral = flowerMoral;
	}

	public String getFlowerIntroduction() {
		return flowerIntroduction;
	}

	public void setFlowerIntroduction(String flowerIntroduction) {
		this.flowerIntroduction = flowerIntroduction;
	}

	@Override
	public String toString() {
		return "FlowerEntity{" +
				"flowerId=" + flowerId +
				", flowerName='" + flowerName + '\'' +
				", flowerType='" + flowerType + '\'' +
				", flowerAlias='" + flowerAlias + '\'' +
				", flowerMoral='" + flowerMoral + '\'' +
				", flowerIntroduction='" + flowerIntroduction + '\'' +
				'}';
	}
}