package com.asianrapid.model.sys;

public class Customer {

    private Long customerId;

    private String orderId1;

    private String customerName;

    private String customerSex;

    private String customerPhone;

    private String customerAddress;

    private String delFlag;

    public Customer() {
    }

    public Customer(Long customerId, String orderId1, String customerName, String customerSex, String customerPhone, String customerAddress, String delFlag) {
        this.customerId = customerId;
        this.orderId1 = orderId1;
        this.customerName = customerName;
        this.customerSex = customerSex;
        this.customerPhone = customerPhone;
        this.customerAddress = customerAddress;
        this.delFlag = delFlag;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getOrderId1() {
        return orderId1;
    }

    public void setOrderId1(String orderId1) {
        this.orderId1 = orderId1;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSex() {
        return customerSex;
    }

    public void setCustomerSex(String customerSex) {
        this.customerSex = customerSex;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", orderId1='" + orderId1 + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerSex='" + customerSex + '\'' +
                ", customerPhone='" + customerPhone + '\'' +
                ", customerAddress='" + customerAddress + '\'' +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }
}
