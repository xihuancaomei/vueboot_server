package com.asianrapid.model.sys;

/**
 * @autohr lzd
 * @description
 * @date 2020/9/9
 */
public class DrinksEntity {
    private Integer drinksId;
    private String drinksName;
    private String drinksType;
    private double drinksPrice;
    private String drinksCapacity;
    private String drinksRemarks;
    private String drinksAddress;
    private String del_flg;

    public Integer getDrinksId() {
        return drinksId;
    }

    public void setDrinksId(Integer drinksId) {
        this.drinksId = drinksId;
    }

    public String getDrinksName() {
        return drinksName;
    }

    public void setDrinksName(String drinksName) {
        this.drinksName = drinksName;
    }

    public String getDrinksType() {
        return drinksType;
    }

    public void setDrinksType(String drinksType) {
        this.drinksType = drinksType;
    }

    public double getDrinksPrice() {
        return drinksPrice;
    }

    public void setDrinksPrice(double drinksPrice) {
        this.drinksPrice = drinksPrice;
    }

    public String getDrinksCapacity() {
        return drinksCapacity;
    }

    public void setDrinksCapacity(String drinksCapacity) {
        this.drinksCapacity = drinksCapacity;
    }

    public String getDrinksRemarks() {
        return drinksRemarks;
    }

    public void setDrinksRemarks(String drinksRemarks) {
        this.drinksRemarks = drinksRemarks;
    }

    public String getDrinksAddress() {
        return drinksAddress;
    }

    public void setDrinksAddress(String drinksAddress) {
        this.drinksAddress = drinksAddress;
    }

    public String getDel_flg() {
        return del_flg;
    }

    public void setDel_flg(String del_flg) {
        this.del_flg = del_flg;
    }
}
