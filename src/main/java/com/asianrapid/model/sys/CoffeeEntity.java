package com.asianrapid.model.sys;

import java.util.Date;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/3
 */
public class CoffeeEntity {
    private Long coffeeId;
    private String coffeeName;
    private String coffeeAddSugar;
    private String status;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;

    public Long getCoffeeId() {
        return coffeeId;
    }

    public void setCoffeeId(Long coffeeId) {
        this.coffeeId = coffeeId;
    }

    public String getCoffeeName() {
        return coffeeName;
    }

    public void setCoffeeName(String coffeeName) {
        this.coffeeName = coffeeName;
    }



    public String getCoffeeAddSugar() {
        return coffeeAddSugar;
    }

    public void setCoffeeAddSugar(String coffeeAddSugar) {
        this.coffeeAddSugar = coffeeAddSugar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CoffeeEntity{" +
                "coffeeId=" + coffeeId +
                ", coffeeName='" + coffeeName + '\'' +
                ", coffeeAddSugar='" + coffeeAddSugar + '\'' +
                ", status='" + status + '\'' +
                ", createBy='" + createBy + '\'' +
                ", createTime=" + createTime +
                ", updateBy='" + updateBy + '\'' +
                ", updateTime=" + updateTime +
                '}';
    }
}
