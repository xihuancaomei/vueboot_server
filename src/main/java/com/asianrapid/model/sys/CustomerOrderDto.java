package com.asianrapid.model.sys;


import java.math.BigDecimal;

public class CustomerOrderDto {

    private Long customerId;

    private String orderId1;

    private String customerName;

    private String customerSex;

    private String customerPhone;

    private String customerAddress;

    private Long orderId;

    private String orderName;

    private Integer orderNumber;

    private BigDecimal orderUnitPrice;

    private BigDecimal orderTotalPrice;

    private String delFlag;

    public CustomerOrderDto() {
    }

    public CustomerOrderDto(Long customerId, String orderId1, String customerName, String customerSex, String customerPhone, String customerAddress, Long orderId, String orderName, Integer orderNumber, BigDecimal orderUnitPrice, BigDecimal orderTotalPrice, String delFlag) {
        this.customerId = customerId;
        this.orderId1 = orderId1;
        this.customerName = customerName;
        this.customerSex = customerSex;
        this.customerPhone = customerPhone;
        this.customerAddress = customerAddress;
        this.orderId = orderId;
        this.orderName = orderName;
        this.orderNumber = orderNumber;
        this.orderUnitPrice = orderUnitPrice;
        this.orderTotalPrice = orderTotalPrice;
        this.delFlag = delFlag;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getOrderId1() {
        return orderId1;
    }

    public void setOrderId1(String orderId1) {
        this.orderId1 = orderId1;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSex() {
        return customerSex;
    }

    public void setCustomerSex(String customerSex) {
        this.customerSex = customerSex;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getOrderUnitPrice() {
        return orderUnitPrice;
    }

    public void setOrderUnitPrice(BigDecimal orderUnitPrice) {
        this.orderUnitPrice = orderUnitPrice;
    }

    public BigDecimal getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(BigDecimal orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "CustomerOrderDto{" +
                "customerId=" + customerId +
                ", orderId1='" + orderId1 + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerSex='" + customerSex + '\'' +
                ", customerPhone='" + customerPhone + '\'' +
                ", customerAddress='" + customerAddress + '\'' +
                ", orderId=" + orderId +
                ", orderName='" + orderName + '\'' +
                ", orderNumber=" + orderNumber +
                ", orderUnitPrice=" + orderUnitPrice +
                ", orderTotalPrice=" + orderTotalPrice +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }
}
