package com.asianrapid.model.sys;

import java.math.BigDecimal;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/3
 */
public class CarEntity {
    private Long carId;
    private String carType;
    private String carExhaust;
    private String carTransmission;
    private BigDecimal carPrice;
    private String carOrign;
    private String delFlag;

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCarExhaust() {
        return carExhaust;
    }

    public void setCarExhaust(String carExhaust) {
        this.carExhaust = carExhaust;
    }

    public String getCarTransmission() {
        return carTransmission;
    }

    public void setCarTransmission(String carTransmission) {
        this.carTransmission = carTransmission;
    }

    public BigDecimal getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(BigDecimal carPrice) {
        this.carPrice = carPrice;
    }

    public String getCarOrign() {
        return carOrign;
    }

    public void setCarOrign(String carOrign) {
        this.carOrign = carOrign;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "CarEntity{" +
                "carId=" + carId +
                ", carType='" + carType + '\'' +
                ", carExhaust='" + carExhaust + '\'' +
                ", carTransmission='" + carTransmission + '\'' +
                ", carPrice=" + carPrice +
                ", carOrign='" + carOrign + '\'' +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }
}
