package com.asianrapid.model.sys;

import java.math.BigDecimal;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/9
 */
public class ARCarEntity {
    private Long carId;
    private String carType;
    private String carExhaust;
    private String carTrans;
    private BigDecimal carPrice;
    private String carOrign;
    private String delFlag;

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCarExhaust() {
        return carExhaust;
    }

    public void setCarExhaust(String carExhaust) {
        this.carExhaust = carExhaust;
    }

    public String getCarTrans() {
        return carTrans;
    }

    public void setCarTrans(String carTrans) {
        this.carTrans = carTrans;
    }

    public BigDecimal getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(BigDecimal carPrice) {
        this.carPrice = carPrice;
    }

    public String getCarOrign() {
        return carOrign;
    }

    public void setCarOrign(String carOrign) {
        this.carOrign = carOrign;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "ARCarEntity{" +
                "carId=" + carId +
                ", carType='" + carType + '\'' +
                ", carExhaust='" + carExhaust + '\'' +
                ", carTrans='" + carTrans + '\'' +
                ", carPrice=" + carPrice +
                ", carOrign='" + carOrign + '\'' +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }
}
