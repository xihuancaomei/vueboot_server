package com.asianrapid.model.sys;

/**
 * @autohr lx
 * @description
 * @date 2020/9/7
 */
public class PaintingEntity {
    /**
     * 画作id
     */
    private int paintingId;
    /**
     * 画作名
     */
    private String paintingName;
    /**
     * 画作类型
     */
    private String paintingType;
    /**
     * 画作作者
     */
    private String paintingAuthor;
    /**
     * 画作收藏地
     */
    private String paintingLocation;
    /**
     * 画作主题
     */
    private String paintingTheme;
    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public int getPaintingId() {
        return paintingId;
    }

    public void setPaintingId(int paintingId) {
        this.paintingId = paintingId;
    }

    public String getPaintingName() {
        return paintingName;
    }

    public void setPaintingName(String paintingName) {
        this.paintingName = paintingName;
    }

    public String getPaintingType() {
        return paintingType;
    }

    public void setPaintingType(String paintingType) {
        this.paintingType = paintingType;
    }

    public String getPaintingAuthor() {
        return paintingAuthor;
    }

    public void setPaintingAuthor(String paintingAuthor) {
        this.paintingAuthor = paintingAuthor;
    }

    public String getPaintingLocation() {
        return paintingLocation;
    }

    public void setPaintingLocation(String paintingLocation) {
        this.paintingLocation = paintingLocation;
    }

    public String getPaintingTheme() {
        return paintingTheme;
    }

    public void setPaintingTheme(String paintingTheme) {
        this.paintingTheme = paintingTheme;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "PaintingEntity{" +
                "paintingId=" + paintingId +
                ", paintingName='" + paintingName + '\'' +
                ", paintingType='" + paintingType + '\'' +
                ", paintingAuthor='" + paintingAuthor + '\'' +
                ", paintingLocation='" + paintingLocation + '\'' +
                ", paintingTheme='" + paintingTheme + '\'' +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }
}
