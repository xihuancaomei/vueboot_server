package com.asianrapid.model.sys;

import java.math.BigDecimal;

/**
 * @autohr bh
 * @description
 * @date 2020/9/3
 */
public class BookEntity {
    /** 图书ID */
    private Long bookId;

    /** 图书名称 */
    private String bookName;

    /** 图书作者 */
    private String bookAuthor;

    /** 图书价格 */
    private BigDecimal bookPrice;

    /** 出版社 */
    private String bookPress;

    /** 图书简介 */
    private String bookSynopsis;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public BigDecimal getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(BigDecimal bookPrice) {
        this.bookPrice = bookPrice;
    }

    public String getBookPress() {
        return bookPress;
    }

    public void setBookPress(String bookPress) {
        this.bookPress = bookPress;
    }

    public String getBookSynopsis() {
        return bookSynopsis;
    }

    public void setBookSynopsis(String bookSynopsis) {
        this.bookSynopsis = bookSynopsis;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "bookId=" + bookId +
                ", bookName='" + bookName + '\'' +
                ", bookAuthor='" + bookAuthor + '\'' +
                ", bookPrice=" + bookPrice +
                ", bookPress='" + bookPress + '\'' +
                ", bookSynopsis='" + bookSynopsis + '\'' +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }
}
