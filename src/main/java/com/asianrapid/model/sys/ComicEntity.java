package com.asianrapid.model.sys;

public class ComicEntity {
    private int comicId;
    private String comicName;
    private String comicAuthor;
    private String comicCategory;
    private String comicState;

    public int getComicId() {
        return comicId;
    }

    public void setComicId(int comicId) {
        this.comicId = comicId;
    }

    public String getComicName() {
        return comicName;
    }

    public void setComicName(String comicName) {
        this.comicName = comicName;
    }

    public String getComicAuthor() {
        return comicAuthor;
    }

    public void setComicAuthor(String comicAuthor) {
        this.comicAuthor = comicAuthor;
    }

    public String getComicCategory() {
        return comicCategory;
    }

    public void setComicCategory(String comicCategory) {
        this.comicCategory = comicCategory;
    }

    public String getComicState() {
        return comicState;
    }

    public void setComicState(String comicState) {
        this.comicState = comicState;
    }

    @Override
    public String toString() {
        return "ComicEntity{" +
                "comicId=" + comicId +
                ", comicName='" + comicName + '\'' +
                ", comicAuthor='" + comicAuthor + '\'' +
                ", comicCategory='" + comicCategory + '\'' +
                ", comicState='" + comicState + '\'' +
                '}';
    }
}
