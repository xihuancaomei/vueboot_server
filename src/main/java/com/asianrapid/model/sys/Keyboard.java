package com.asianrapid.model.sys;

import java.math.BigDecimal;

public class Keyboard {

    private Long keyboardId;

    private String keyboardName;

    private String keyboardBrand;

    private String keyboardCat;

    private BigDecimal keyboardPrice;

    public Keyboard() {

    }

    public Keyboard(Long keyboardId, String keyboardName, String keyboardBrand, String keyboardCat, BigDecimal keyboardPrice) {
        this.keyboardId = keyboardId;
        this.keyboardName = keyboardName;
        this.keyboardBrand = keyboardBrand;
        this.keyboardCat = keyboardCat;
        this.keyboardPrice = keyboardPrice;
    }

    public Long getKeyboardId() {
        return keyboardId;
    }

    public void setKeyboardId(Long keyboardId) {
        this.keyboardId = keyboardId;
    }

    public String getKeyboardName() {
        return keyboardName;
    }

    public void setKeyboardName(String keyboardName) {
        this.keyboardName = keyboardName;
    }

    public String getKeyboardBrand() {
        return keyboardBrand;
    }

    public void setKeyboardBrand(String keyboardBrand) {
        this.keyboardBrand = keyboardBrand;
    }

    public String getKeyboardCat() {
        return keyboardCat;
    }

    public void setKeyboardCat(String keyboardCat) {
        this.keyboardCat = keyboardCat;
    }

    public BigDecimal getKeyboardPrice() {
        return keyboardPrice;
    }

    public void setKeyboardPrice(BigDecimal keyboardPrice) {
        this.keyboardPrice = keyboardPrice;
    }

    @Override
    public String toString() {
        return "KeyboardEntity{" +
                "keyboardId=" + keyboardId +
                ", keyboardName='" + keyboardName + '\'' +
                ", keyboardBrand='" + keyboardBrand + '\'' +
                ", keyboardCat='" + keyboardCat + '\'' +
                ", keyboardPrice=" + keyboardPrice +
                '}';
    }

}
