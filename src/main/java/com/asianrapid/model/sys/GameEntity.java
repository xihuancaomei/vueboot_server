package com.asianrapid.model.sys;

/**
 * @autohr lx
 * @description
 * @date 2020/9/3
 */
public class GameEntity {
    /**
     * 游戏id
     */
    private int gameId;
    /**
     * 游戏名
     */
    private String gameName;
    /**
     * 游戏类型
     */
    private String gameType;
    /**
     * 游戏公司
     */
    private String gameCompany;
    /**
     * 游戏地址
     */
    private String gameAddress;
    /**
     * 游戏备注名
     */
    private String remark;

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getGameCompany() {
        return gameCompany;
    }

    public void setGameCompany(String gameCompany) {
        this.gameCompany = gameCompany;
    }

    public String getGameAddress() {
        return gameAddress;
    }

    public void setGameAddress(String gameAddress) {
        this.gameAddress = gameAddress;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "GameEntity{" +
                "gameId=" + gameId +
                ", gameName='" + gameName + '\'' +
                ", gameType='" + gameType + '\'' +
                ", gameCompany='" + gameCompany + '\'' +
                ", gameAddress='" + gameAddress + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
