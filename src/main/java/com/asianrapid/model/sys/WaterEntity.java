package com.asianrapid.model.sys;

import java.math.BigDecimal;

/**
 * @autohr lzd
 * @description
 * @date 2020/9/7
 */
public class WaterEntity {
    private Integer waterId;
    private String waterName;
    private  String waterType;
    private double waterPrice;
    private  String waterCapacity;
    private String waterRemarks;
    private String waterAddress;

    public Integer getWaterId() {
        return waterId;
    }

    public void setWaterId(Integer waterId) {
        this.waterId = waterId;
    }

    public String getWaterName() {
        return waterName;
    }

    public void setWaterName(String waterName) {
        this.waterName = waterName;
    }

    public String getWaterType() {
        return waterType;
    }

    public void setWaterType(String waterType) {
        this.waterType = waterType;
    }

    public double getWaterPrice() {
        return waterPrice;
    }

    public void setWaterPrice(double waterPrice) {
        this.waterPrice = waterPrice;
    }

    public String getWaterCapacity() {
        return waterCapacity;
    }

    public void setWaterCapacity(String waterCapacity) {
        this.waterCapacity = waterCapacity;
    }

    public String getWaterRemarks() {
        return waterRemarks;
    }

    public void setWaterRemarks(String waterRemarks) {
        this.waterRemarks = waterRemarks;
    }

    public String getWaterAddress() {
        return waterAddress;
    }

    public void setWaterAddress(String waterAddress) {
        this.waterAddress = waterAddress;
    }

    @Override
    public String toString() {
        return "WaterEntity{" +
                "waterId=" + waterId +
                ", waterName='" + waterName + '\'' +
                ", waterType='" + waterType + '\'' +
                ", waterPrice=" + waterPrice +
                ", waterCapacity='" + waterCapacity + '\'' +
                ", waterRemarks='" + waterRemarks + '\'' +
                ", waterAddress='" + waterAddress + '\'' +
                '}';
    }
}
