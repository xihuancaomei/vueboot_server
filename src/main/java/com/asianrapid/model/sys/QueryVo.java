package com.asianrapid.model.sys;

public class QueryVo {

    private String orderId;

    private Long customerId;

    private Order order;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public QueryVo(String orderId, Long customerId) {
        this.orderId = orderId;
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "QueryVo{" +
                "orderId='" + orderId + '\'' +
                ", customerId=" + customerId +
                ", order=" + order +
                '}';
    }
}
