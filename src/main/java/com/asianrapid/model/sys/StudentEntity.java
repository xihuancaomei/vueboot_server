package com.asianrapid.model.sys;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/7
 */
public class StudentEntity {
    private Long stdId;
    private String stdName;
    private Long stdNumber;
    private String stdGender;
    private String stdRoom;
    private String stdClass;
    private String stdTeacher;
    private String delFlag;
    private String remark;

    public Long getStdId() {
        return stdId;
    }

    public void setStdId(Long stdId) {
        this.stdId = stdId;
    }

    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public Long getStdNumber() {
        return stdNumber;
    }

    public void setStdNumber(Long stdNumber) {
        this.stdNumber = stdNumber;
    }

    public String getStdGender() {
        return stdGender;
    }

    public void setStdGender(String stdGender) {
        this.stdGender = stdGender;
    }

    public String getStdRoom() {
        return stdRoom;
    }

    public void setStdRoom(String stdRoom) {
        this.stdRoom = stdRoom;
    }

    public String getStdClass() {
        return stdClass;
    }

    public void setStdClass(String stdClass) {
        this.stdClass = stdClass;
    }

    public String getStdTeacher() {
        return stdTeacher;
    }

    public void setStdTeacher(String stdTeacher) {
        this.stdTeacher = stdTeacher;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "StudentEntity{" +
                "stdId=" + stdId +
                ", stdName='" + stdName + '\'' +
                ", stdNumber=" + stdNumber +
                ", stdGender='" + stdGender + '\'' +
                ", stdRoom='" + stdRoom + '\'' +
                ", stdClass='" + stdClass + '\'' +
                ", stdTeacher='" + stdTeacher + '\'' +
                ", delFlag='" + delFlag + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
