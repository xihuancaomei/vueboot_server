package com.asianrapid.model.sys;

import java.util.Date;

/**
 * @autohr gx
 * @description
 * @date 2020/9/3
 */
public class Film {

    private Long id;
    private String name;
    private String star;
    private String type;
    private String bo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBo() {
        return bo;
    }

    public void setBo(String bo) {
        this.bo = bo;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", star='" + star + '\'' +
                ", type='" + type + '\'' +
                ", bo='" + bo + '\'' +
                '}';
    }
}
