package com.asianrapid.model.sys;

public class CartoonEntity {
    private int cartoonId;
    private String cartoonName;
    private String cartoonAuthor;
    private String cartoonCategory;
    private String cartoonState;

    public int getCartoonId() {
        return cartoonId;
    }

    public void setCartoonId(int cartoonId) {
        this.cartoonId = cartoonId;
    }

    public String getCartoonName() {
        return cartoonName;
    }

    public void setCartoonName(String cartoonName) {
        this.cartoonName = cartoonName;
    }

    public String getCartoonAuthor() {
        return cartoonAuthor;
    }

    public void setCartoonAuthor(String cartoonAuthor) {
        this.cartoonAuthor = cartoonAuthor;
    }

    public String getCartoonCategory() {
        return cartoonCategory;
    }

    public void setCartoonCategory(String cartoonCategory) {
        this.cartoonCategory = cartoonCategory;
    }

    public String getCartoonState() {
        return cartoonState;
    }

    public void setCartoonState(String cartoonState) {
        this.cartoonState = cartoonState;
    }

    @Override
    public String toString() {
        return "CartoonEntity{" +
                "cartoonId=" + cartoonId +
                ", cartoonName='" + cartoonName + '\'' +
                ", cartoonAuthor='" + cartoonAuthor + '\'' +
                ", cartoonCategory='" + cartoonCategory + '\'' +
                ", cartoonState='" + cartoonState + '\'' +
                '}';
    }
}
