package com.asianrapid.model.sys;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/7
 */
public class AppEntity {
    private Long appId;
    private String appName;
    private String appType;
    private String appOnline;
    private String appCharge;

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getAppOnline() {
        return appOnline;
    }

    public void setAppOnline(String appOnline) {
        this.appOnline = appOnline;
    }

    public String getAppCharge() {
        return appCharge;
    }

    public void setAppCharge(String appCharge) {
        this.appCharge = appCharge;
    }

    @Override
    public String toString() {
        return "AppEntity{" +
                "appId=" + appId +
                ", appName='" + appName + '\'' +
                ", appType='" + appType + '\'' +
                ", appOnline='" + appOnline + '\'' +
                ", appCharge='" + appCharge + '\'' +
                '}';
    }
}
