package com.asianrapid.model.sys;

import java.util.List;

public class ToyEntity {
	/**
	 * id
	 */
	private int id;
	/**
	 * 菜单名
	 */
	private String name;
	/**
	 * role
	 */
	private String effect;
	/**
	 * 热度
	 */
	private int popular;
	/**
	 * 排序
	 */
	private int sort;
	/**
	 * 描述
	 */
	private String remark;
	/**
	 * 图标
	 */
	private String icon;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public int getPopular() {
		return popular;
	}

	public void setPopular(int popular) {
		this.popular = popular;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}


	@Override
	public String toString() {
		return "ToyEntity{" +
				"id=" + id +
				", name='" + name + '\'' +
				", effect='" + effect + '\'' +
				", popular=" + popular +
				", sort=" + sort +
				", remark='" + remark + '\'' +
				", icon='" + icon + '\'' +
				'}';
	}
}
