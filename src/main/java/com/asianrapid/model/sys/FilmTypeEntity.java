package com.asianrapid.model.sys;

/**
 * @autohr gx
 * @description
 * @date 2020/9/9
 */
public class FilmTypeEntity {

    private Long typeId;
    private String typeName;

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "FilmTypeEntity{" +
                "typeId=" + typeId +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
