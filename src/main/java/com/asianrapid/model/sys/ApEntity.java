package com.asianrapid.model.sys;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/8
 */
public class ApEntity {
    private Long apId;
    private String apName;
    private String apType;
    private String apOnline;
    private String apCharge;

    public Long getApId() {
        return apId;
    }

    public void setApId(Long apId) {
        this.apId = apId;
    }

    public String getApName() {
        return apName;
    }

    public void setApName(String apName) {
        this.apName = apName;
    }

    public String getApType() {
        return apType;
    }

    public void setApType(String apType) {
        this.apType = apType;
    }

    public String getApOnline() {
        return apOnline;
    }

    public void setApOnline(String apOnline) {
        this.apOnline = apOnline;
    }

    public String getApCharge() {
        return apCharge;
    }

    public void setApCharge(String apCharge) {
        this.apCharge = apCharge;
    }

    @Override
    public String toString() {
        return "ApEntity{" +
                "apId=" + apId +
                ", apName='" + apName + '\'' +
                ", apType='" + apType + '\'' +
                ", apOnline='" + apOnline + '\'' +
                ", apCharge='" + apCharge + '\'' +
                '}';
    }
}
