package com.asianrapid.model.sys;

public class PlantEntity {
	private int plantId;
	private String plantName;
	private String plantType;
	private String plantAlias;
	private String plantMoral;
	private String plantIntroduction;

	public int getPlantId() {
		return plantId;
	}

	public void setPlantId(int plantId) {
		this.plantId = plantId;
	}

	public String getPlantName() {
		return plantName;
	}

	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}

	public String getPlantType() {
		return plantType;
	}

	public void setPlantType(String plantType) {
		this.plantType = plantType;
	}

	public String getPlantAlias() {
		return plantAlias;
	}

	public void setPlantAlias(String plantAlias) {
		this.plantAlias = plantAlias;
	}

	public String getPlantMoral() {
		return plantMoral;
	}

	public void setPlantMoral(String plantMoral) {
		this.plantMoral = plantMoral;
	}

	public String getPlantIntroduction() {
		return plantIntroduction;
	}

	public void setPlantIntroduction(String plantIntroduction) {
		this.plantIntroduction = plantIntroduction;
	}

	@Override
	public String toString() {
		return "PlantEntity{" +
				"plantId=" + plantId +
				", plantName='" + plantName + '\'' +
				", plantType='" + plantType + '\'' +
				", plantAlias='" + plantAlias + '\'' +
				", plantMoral='" + plantMoral + '\'' +
				", plantIntroduction='" + plantIntroduction + '\'' +
				'}';
	}
}