package com.asianrapid.model.sys;

/**
 * @autohr bh
 * @description
 * @date 2020/9/7
 */
public class OnlineGameEntity {
    /**
     * 游戏id
     */
    private Long gameId;
    /**
     * 游戏名
     */
    private String gameName;
    /**
     * 游戏类型
     */
    private String gameType;
    /**
     * 游戏公司
     */
    private String gameCompany;
    /**
     * 游戏运营
     */
    private String gameOperation;
    /**
     * 游戏简介
     */
    private String gameSynopsis;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getGameCompany() {
        return gameCompany;
    }

    public void setGameCompany(String gameCompany) {
        this.gameCompany = gameCompany;
    }

    public String getGameOperation() {
        return gameOperation;
    }

    public void setGameOperation(String gameOperation) {
        this.gameOperation = gameOperation;
    }

    public String getGameSynopsis() {
        return gameSynopsis;
    }

    public void setGameSynopsis(String gameSynopsis) {
        this.gameSynopsis = gameSynopsis;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "OnlineGameEntity{" +
                "gameId=" + gameId +
                ", gameName='" + gameName + '\'' +
                ", gameType='" + gameType + '\'' +
                ", gameCompany='" + gameCompany + '\'' +
                ", gameOperation='" + gameOperation + '\'' +
                ", gameSynopsis='" + gameSynopsis + '\'' +
                '}';
    }
}
