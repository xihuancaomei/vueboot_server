package com.asianrapid.controller.sys;


import com.asianrapid.model.sys.MenuEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.model.sys.ToyEntity;
import com.asianrapid.service.sys.ToyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ToyController {

	private Logger log = LoggerFactory.getLogger(ToyController.class);

	@Resource(name = "toyServiceImpl")
	private ToyService toyService;

	@GetMapping("/toys/name")
	public List<ToyEntity> nameList() {
		return toyService.nameList();
	}

	@GetMapping("/toys")
	public PageResult toysList(int pageSize, String name, String effect, int page) {
		PageResult pageResult = new PageResult();
		pageResult.setData(toyService.toysList(pageSize, name, effect, page * pageSize));
		pageResult.setTotalCount(toyService.toysSize(pageSize, name, effect, page * pageSize));
		log.debug("The method is ending");
		return pageResult;
	}

	@PostMapping("/toys/toy")
	public ToyEntity insertToy(@RequestBody ToyEntity toyEntity) {
		toyService.insertToy(toyEntity);
		log.debug("The method is ending");
		return toyEntity;
	}

	@PutMapping("/toys/{id}")
	public ToyEntity updateToy(@RequestBody ToyEntity toyEntity, @PathVariable int id) {
		if (toyEntity.getId() == id) {
			toyService.updateToy(toyEntity);
		}
		log.debug("The method is ending");
		return toyEntity;
	}

	@DeleteMapping("/toys")
	public List<String> deleteToys(@RequestBody List<String> groupId) {
		toyService.deleteToys(groupId);
		return groupId;
	}
}
