package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.BookEntity;
import com.asianrapid.model.sys.OnLineGameDictEntity;
import com.asianrapid.model.sys.OnlineGameEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.OnlineGameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @autohr bh
 * @description
 * @date 2020/9/7
 */
@RestController
public class OnlineGameController {

    private Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    OnlineGameService onlineGameService;

    /**
     * 获取online_game表数据
     */
    @GetMapping("/OLgames")
    public PageResult gamesList(String gameName, String gameType, String gameOperation, int pageSize, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(onlineGameService.onlineGamesList(gameName, gameType, gameOperation, pageSize, page * pageSize));
        pageResult.setTotalCount(onlineGameService.onlineGamesSize(gameName, gameType, gameOperation, pageSize, page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }
    /**
     * 新增online_game表数据
     */
    @PostMapping("/OLgames/add")
    public OnlineGameEntity insertOnlineGames(@RequestBody OnlineGameEntity onlineGameEntity) {
        onlineGameService.insertOnlineGames(onlineGameEntity);
        log.debug("The method is ending");
        return onlineGameEntity;
    }

    /**
     * 更新online_game信息
     */
    @PutMapping("/OLgames/{id}")
    public OnlineGameEntity updateOnlineGames(@RequestBody OnlineGameEntity onlineGameEntity, @PathVariable Long id) {
        if (onlineGameEntity.getGameId() == id) {
            onlineGameService.updateOnlineGames(onlineGameEntity);
        }
        log.debug("The method is ending");
        return onlineGameEntity;
    }
    /**
     * 删除online_game信息
     */
    @DeleteMapping("/OLgames")
    public List<String> deleteOnlineGames(@RequestBody List<String> groupId) {
        onlineGameService.deleteOnlineGames(groupId);
        return groupId;
    }

    /**
     * 获取全部下拉数据
     */
    @GetMapping("/OLgames/operation")
    public List<OnLineGameDictEntity> operationList(){
        return onlineGameService.selectOperationList();
    }

    @GetMapping("/OLgames/type")
    public List<OnLineGameDictEntity> typeList(){

        return onlineGameService.selectTypeList();
    }
}
