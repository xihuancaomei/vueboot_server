package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.ShoesEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.ShoesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr gx
 * @description
 * @date 2020/9/7
 */
@RestController
public class ShoesController {

    private Logger log = LoggerFactory.getLogger(MenuController.class);

    @Resource(name = "shoesServiceImpl")
    private ShoesService shoesService;

    /**
     * 列表展示
     */
    @GetMapping("/shoes")
    public PageResult shoesList(int pageSize, int page, String type, String brand) {
        PageResult pageResult = new PageResult();
        pageResult.setData(shoesService.shoesList(pageSize, page * pageSize, type,brand));
        pageResult.setTotalCount(shoesService.shoesSize(pageSize, page * pageSize,type,brand));
        log.debug("The method is ending");
        return pageResult;
    }

    /**
     * 新增电影
     */
    @PostMapping("/shoes/insert")
    public ShoesEntity insertShoes(@RequestBody ShoesEntity shoes) {
        shoesService.insertShoes(shoes);
        log.debug("The method is ending");
        return shoes;
    }

    /**
     * 返显
     */
    @GetMapping("/shoes/all")
    public List<ShoesEntity> allShoes(){
        return shoesService.allShoes();
    }

    /**
     * 修改电影
     */
    @PutMapping("/shoes/{id}")
    public ShoesEntity updateShoes(@RequestBody ShoesEntity shoesEntity, @PathVariable int id) {
        if (shoesEntity.getId() == id) {
            shoesService.updateShoes(shoesEntity);
        }
        log.debug("The method is ending");
        return shoesEntity;
    }
    /**
     * 删除电影
     */
    @DeleteMapping("/shoes")
    public List<String> deleteShoes(@RequestBody List<String> groupId) {
        shoesService.deleteShoes(groupId);
        return groupId;
    }
    /**
     * 下拉框
     */
    @GetMapping("/shoes/type")
    public List<ShoesEntity> shoesByType() {
        return shoesService.shoesByType();
    }
}
