package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.PageResult;
import com.asianrapid.model.sys.SmokeEntity;
import com.asianrapid.service.sys.SmokeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @autohr lzd
 * @description
 * @date 2020/9/3
 */
@RestController
public class SmokeController {
    private Logger log = LoggerFactory.getLogger(SmokeController.class);

    @Autowired
    private SmokeService smokeService;
    @GetMapping("/smoke/all")
    public List<SmokeEntity> type() {
        List<SmokeEntity> smokeTypeList = smokeService.getSmokeTypeList();
        return smokeTypeList;
    }

    @GetMapping("/smokes")
    public PageResult list(String smokeTar,String smokeType,String smokeName, int pageSize, int page) {
        PageResult PageResult =  new PageResult();
        PageResult.setData(smokeService.getSmokeList(smokeTar,smokeType,smokeName, pageSize, page * pageSize));
        PageResult.setTotalCount(smokeService.getSmokeListSize(smokeTar,smokeType,smokeName,pageSize, page * pageSize));
        log.debug("The method is ending");
        return PageResult;
    }

    @PostMapping("smokes/smoke")
    public SmokeEntity add(@RequestBody SmokeEntity smokeEntity) {
        smokeService.insertSmoke(smokeEntity);
        log.debug("The method is ending");
        return smokeEntity;
    }
    @PutMapping("/smokes/{smokeId}")
    public SmokeEntity edit(@RequestBody SmokeEntity smokeEntity, @PathVariable int smokeId) {
        if(smokeEntity.getSmokeId() == smokeId) {
            smokeService.updateSmokes(smokeEntity);
        }
        log.debug("The method is ending");
        return smokeEntity;
    }
    @DeleteMapping("/smokes")
    public List<String> delete(@RequestBody List<String> groupId) {
        smokeService.deleteSmoke(groupId);
        log.debug("The method is ending");
        return groupId;
    }
}
