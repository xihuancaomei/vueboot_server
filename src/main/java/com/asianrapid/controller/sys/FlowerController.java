package com.asianrapid.controller.sys;


import com.asianrapid.model.sys.FlowerEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.model.sys.ToyEntity;
import com.asianrapid.service.sys.FlowerService;
import com.asianrapid.service.sys.ToyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class FlowerController {

	private Logger log = LoggerFactory.getLogger(FlowerController.class);

	@Resource(name = "flowerServiceImpl")
	private FlowerService flowerService;

	@GetMapping("/flowers/flowerType")
	public List<FlowerEntity>flowerTypeList(){
		List<FlowerEntity> flowerEntity = flowerService.flowerTypeList();
		return flowerEntity;
	}

	@GetMapping("/flowers")
	public PageResult flowersList(int pageSize, String flowerType, String flowerName, int page) {
		PageResult pageResult = new PageResult();
		pageResult.setData(flowerService.flowersList(pageSize, flowerType, flowerName, page * pageSize));
		pageResult.setTotalCount(flowerService.flowersSize(pageSize, flowerType, flowerName, page * pageSize));
		log.debug("The method is ending");
		return pageResult;
	}

	@PostMapping("/flowers/flower")
	public FlowerEntity insertFlower(@RequestBody FlowerEntity flowerEntity) {
		flowerService.insertFlower(flowerEntity);
		log.debug("The method is ending");
		return flowerEntity;
	}

	@PutMapping("/flowers/{flowerId}")
	public FlowerEntity updateFlower(@RequestBody FlowerEntity flowerEntity, @PathVariable int flowerId) {
		if (flowerEntity.getFlowerId() == flowerId) {
			flowerService.updateFlower(flowerEntity);
		}
		log.debug("The method is ending");
		return flowerEntity;
	}

	@DeleteMapping("/flowers")
	public List<String> deleteFlowers(@RequestBody List<String> groupId) {
		flowerService.deleteFlowers(groupId);
		return groupId;
	}
}