package com.asianrapid.controller.sys;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/4
 */

import com.asianrapid.model.sys.ComicEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.ComicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ComicController {

    private Logger log = LoggerFactory.getLogger(ComicController.class);

    @Resource(name = "comicServiceImpl")
    private ComicService comicService;

    @GetMapping("/comics/comicCategory")
    public List<ComicEntity> comicList() {
        return comicService.comicList();
    }


    @GetMapping("/comics")
    public PageResult comicsList(int pageSize, String comicCategory, String comicName, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(comicService.comicsList(pageSize,  comicCategory, comicName,page * pageSize));
        pageResult.setTotalCount(comicService.comicsSize(pageSize,  comicCategory, comicName,page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }

    @PostMapping("/comics/comic")
    public ComicEntity insertComic(@RequestBody ComicEntity comicEntity) {
        comicService.insertComic(comicEntity);
        log.debug("The method is ending");
        return comicEntity;
    }

    @PutMapping("/comics/{comicId}")
    public ComicEntity updateComic(@RequestBody ComicEntity comicEntity, @PathVariable int comicId) {
        if (comicEntity.getComicId() == comicId) {
            comicService.updateComic(comicEntity);
        }
        log.debug("The method is ending");
        return comicEntity;
    }

    @DeleteMapping("/comics")
    public List<String> deleteComics(@RequestBody List<String> groupId) {
        comicService.deleteComics(groupId);
        return groupId;
    }

}
