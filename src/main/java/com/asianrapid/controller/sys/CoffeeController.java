package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.CoffeeEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.CoffeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/3
 */
@RestController
public class CoffeeController {
    private Logger log = LoggerFactory.getLogger(CoffeeController.class);

    @Resource(name = "coffeeServiceImpl")
    private CoffeeService coffeeService;

    @GetMapping("/coffees")
    public PageResult coffeesList(int pageSize, String coffeeName,String coffeeAddSugar,int page) {
        System.out.println("我走了这个方法");
        PageResult pageResult = new PageResult();
        pageResult.setData(coffeeService.coffeesList(pageSize, coffeeName,coffeeAddSugar,page * pageSize));
        pageResult.setTotalCount(coffeeService.coffeesSize(pageSize, coffeeName,coffeeAddSugar,page * pageSize));
        log.debug("The method is ending");
      //  System.out.println(coffeeService.coffeesList(pageSize,page*pageSize));

        return pageResult;
    }

    @PostMapping("/coffees/coffee")
    public CoffeeEntity insertCoffee(@RequestBody CoffeeEntity coffeeEntity) {
        coffeeService.insertCoffee(coffeeEntity);
        log.debug("The method is ending");
        return coffeeEntity;
    }

    @PutMapping("/coffees/{coffeeId}")
    public CoffeeEntity updateCoffee(@RequestBody CoffeeEntity coffeeEntity, @PathVariable int coffeeId) {
        if (coffeeEntity.getCoffeeId() == coffeeId) {
            coffeeService.updateCoffee(coffeeEntity);
        }
        log.debug("The method is ending");
        return coffeeEntity;
    }

    @DeleteMapping("/coffees")
    public List<String> deleteCoffees(@RequestBody List<String> groupId) {
       coffeeService.deleteCoffees(groupId);
        return groupId;
    }

    @GetMapping("/coffees/coffeeName")
    public List<CoffeeEntity>coffeesByName() {
        return coffeeService.coffeesByName();
    }
}
