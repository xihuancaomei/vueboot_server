package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.GameEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr lx
 * @description
 * @date 2020/9/3
 */
@RestController
public class GameController {
    private Logger log = LoggerFactory.getLogger(GameController.class);

    @Resource(name = "gameServiceImpl")
    private GameService gameService;

    /**
     * 获取game表数据
     *
     * @param pageSize
     * @param page
     * @return
     */
    @GetMapping("/game")
    public PageResult gamesList(String gameName,String gameType, int pageSize, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(gameService.gamesList(gameName,gameType,pageSize, page * pageSize));
        pageResult.setTotalCount(gameService.gamesSize(gameName,gameType,pageSize, page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }
    /**
     * 新建角色信息
     *
     * @param gameEntity
     * @return
     */
    @PostMapping("/game/game")
    public GameEntity insertRole(@RequestBody GameEntity gameEntity) {
        gameService.insertGame(gameEntity);
        log.debug("The method is ending");
        return gameEntity;
    }
    /**
     * 删除菜单信息
     *
     * @param groupId
     * @return
     */
    @DeleteMapping("/games")
    public List<String> deleteGames(@RequestBody List<String> groupId) {
        gameService.deleteGames(groupId);
        return groupId;
    }
    /**
     * 更新角色信息
     *
     * @param gameEntity
     * @param gameId
     * @return
     */
    @PutMapping("/games/{gameId}")
    public GameEntity updateRole(@RequestBody GameEntity gameEntity, @PathVariable int gameId) {
        if (gameEntity.getGameId() == gameId) {
            gameService.updateGame(gameEntity);
        }
        log.debug("The method is ending");
        return gameEntity;
    }
    @GetMapping("/games/{gameName}")
    public GameEntity gameGet(@PathVariable String gameName) {
        GameEntity gameEntity = gameService.getGameEntityByGameName(gameName);
        log.debug("The method is ending");
        return gameEntity;
    }
    @GetMapping("/games")
    public List<GameEntity> gameTypeList() {
        return gameService.gameTypeList();
    }
}
