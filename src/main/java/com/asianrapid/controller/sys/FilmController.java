package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.Film;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.FilmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr gx
 * @description
 * @date 2020/9/3
 */
@RestController
public class FilmController {

    private Logger log = LoggerFactory.getLogger(MenuController.class);

    @Resource(name = "filmServiceImpl")
    private FilmService filmService;

    /**
     * 列表展示
     */
    @GetMapping("/film")
    public PageResult filmList( int pageSize, int page,String type,String name) {
        PageResult pageResult = new PageResult();
        pageResult.setData(filmService.filmList(pageSize, page * pageSize, type,name));
        pageResult.setTotalCount(filmService.filmSize(pageSize, page * pageSize,type,name));
        log.debug("The method is ending");
        return pageResult;
    }

    /**
     * 新增电影
     */
    @PostMapping("/film/film")
    public Film insertFilm(@RequestBody Film film) {
        filmService.insertFilm(film);
        log.debug("The method is ending");
        return film;
    }

    /**
     * 返显
     */
    @GetMapping("/film/all")
    public List<Film> allFilm(){
        return filmService.allFilm();
    }

    /**
     * 修改电影
     */
    @PutMapping("/film/{id}")
    public Film updateFilm(@RequestBody Film filmEntity, @PathVariable int id) {
        if (filmEntity.getId() == id) {
            filmService.updateFilm(filmEntity);
        }
        log.debug("The method is ending");
        return filmEntity;
    }
    /**
     * 删除电影
     */
    @DeleteMapping("/film")
    public List<String> deleteFilm(@RequestBody List<String> groupId) {
        filmService.deleteFilm(groupId);
        return groupId;
    }
    /**
     * 下拉框
     */
    @GetMapping("/film/type")
    public List<Film> filmByType() {
        return filmService.filmByType();
    }
}
