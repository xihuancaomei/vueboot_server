package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.*;
import com.asianrapid.service.sys.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@RestController
public class CustomerController {

    @Resource(name = "customerServiceImpl")
    private CustomerService customerService;

    private Logger log = LoggerFactory.getLogger(MouseController.class);

    @GetMapping("/customers")
    public PageResult customersList(String customerName, String orderId, String name, int pageSize, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(customerService.customersList(customerName, orderId, name, pageSize, page * pageSize));
        pageResult.setTotalCount(customerService.customerSize(customerName, orderId, name, pageSize, page * pageSize));
        System.out.println(customerService.customersList(customerName, orderId, name, pageSize, page * pageSize));
        return  pageResult;
    }

    @GetMapping("/customers/order")
    public List<String> selectOrderNameList() {
        return customerService.selectOrderNameList();
    }

    @PostMapping("/customers/customer")
    public CustomerOrderDto insertCustomer(@RequestBody CustomerOrderDto customerOrderDto, QueryVo queryVo) {
        Long unitPrice = customerOrderDto.getOrderUnitPrice().longValue();
        BigDecimal orderTotalPrice = BigDecimal.valueOf(customerOrderDto.getOrderNumber() * unitPrice);
        customerOrderDto.setOrderTotalPrice(orderTotalPrice);
        System.out.println(customerOrderDto);
        customerService.insertCustomer(customerOrderDto);
        queryVo.setCustomerId(customerService.selectCustomerId());
        customerService.insertOrder(customerOrderDto);
        queryVo.setOrderId(customerService.selectOrderId().toString());
        System.out.println(queryVo);
        customerService.insertCustomerOrderId(queryVo);
        log.debug("The method is ending");
        return customerOrderDto;
    }

    @PutMapping("/customers/{customerId}")
    public CustomerOrderDto updateCustomer(@RequestBody CustomerOrderDto customerOrderDto, @PathVariable Long customerId, Order order) {
        if (customerOrderDto.getCustomerId() == customerId) {
            customerService.updateCustomer(customerOrderDto);
        }
        order.setOrderId(customerId);
        order.setOrderName(customerOrderDto.getOrderName());
        order.setOrderNumber(customerOrderDto.getOrderNumber());
        order.setOrderUnitPrice(customerOrderDto.getOrderUnitPrice());
        Long unitPrice = customerOrderDto.getOrderUnitPrice().longValue();
        BigDecimal orderTotalPrice = BigDecimal.valueOf(customerOrderDto.getOrderNumber() * unitPrice);
        order.setOrderTotalPrice(orderTotalPrice);
        System.out.println(order);
        customerService.updateOrder(order);
        log.debug("The method is ending");
        return customerOrderDto;
    }

    @DeleteMapping("/customers")
    public List<String> deleteCustomers(@RequestBody List<String> groupId) {
        customerService.deleteCustomers(groupId);
        return groupId;
    }
}
