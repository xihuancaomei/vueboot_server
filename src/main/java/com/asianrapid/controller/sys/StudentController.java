package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.PageResult;
import com.asianrapid.model.sys.StudentEntity;
import com.asianrapid.service.sys.StudentService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/7
 */
@RestController
public class StudentController {

    @Resource(name = "studentServiceImpl")
    private StudentService studentService;

    @GetMapping("/std")
    public PageResult getStdList(Integer pageSize,Integer page,String stdName,String stdNumber,
                                 String stdRoom,String stdClass,String stdTeacher){
        PageResult pageResult = new PageResult();
        pageResult.setData(studentService.getStdList(stdName,stdNumber,stdRoom,stdClass,stdTeacher,pageSize,page*pageSize));
        pageResult.setTotalCount(studentService.getStdListCount(stdName,stdNumber,stdRoom,stdClass,stdTeacher,pageSize,page*pageSize));
        return pageResult;
    }

    @GetMapping("/std/getStdClassList")
    public List<String> getStdClassList(){
        System.out.println(studentService.getStdClassList());
        return studentService.getStdClassList();
    }

    @GetMapping("/std/getStdTeacherList")
    public List<String> getStdTeacherList(){
        System.out.println(studentService.getStdTeacherList());
        return studentService.getStdTeacherList();
    }

    @PostMapping("/std/insert")
    public void insertStd(@RequestBody StudentEntity std){
        System.out.println("我是插入方法");
        studentService.insertStd(std);
    }

    @PutMapping("/std/{stdId}")
    public void updateStd(@RequestBody StudentEntity std,@PathVariable Long stdId){
        System.out.println(stdId);
        std.setStdId(stdId);
        System.out.println("我是修改方法");
        studentService.updateStd(std);
    }

    @DeleteMapping("/std/delete")
    public void deleteStd(@RequestBody Long[] stdIds){
        System.out.println(stdIds);
        studentService.deleteStd(stdIds);
    }
}
