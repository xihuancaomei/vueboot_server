package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.Keyboard;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.KeyboardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class KeyboardController {

    private Logger log = LoggerFactory.getLogger(MouseController.class);

    @Resource(name = "keyboardServiceImpl")
    private KeyboardService keyboardService;

    @GetMapping("/keyboards")
    public PageResult keyboardsList(String keyboardName, String keyboardBrand, String keyboardCat, int pageSize, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(keyboardService.keyboardsList(keyboardName, keyboardBrand, keyboardCat, pageSize, page * pageSize));
        pageResult.setTotalCount(keyboardService.keyboardSize(keyboardName, keyboardBrand, keyboardCat, pageSize, page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }

    @GetMapping("/keyboards/brandList")
    public String[] brandList(){
        return keyboardService.selectBrandList();
    }

    @GetMapping("/keyboards/catList")
    public String[] catList(){
        return keyboardService.selectCatList();
    }

    @PostMapping("/keyboards/keyboard")
    public Keyboard insertKeyboard(@RequestBody Keyboard keyboard) {
        keyboardService.insertKeyboard(keyboard);
        log.debug("The method is ending");
        return keyboard;
    }

    @PutMapping("/keyboards/{keyboardId}")
    public Keyboard updateKeyboard(@RequestBody Keyboard keyboard, @PathVariable Long keyboardId) {
        if (keyboard.getKeyboardId() == keyboardId) {
            keyboardService.updateKeyboard(keyboard);
        }
        log.debug("The method is ending");
        return keyboard;
    }

    @DeleteMapping("/keyboards")
    public List<String> deleteKeyboards(@RequestBody List<String> groupId) {
        keyboardService.deleteKeyboards(groupId);
        return groupId;
    }

}
