package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.PageResult;
import com.asianrapid.model.sys.WaterEntity;
import com.asianrapid.service.sys.WaterService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @autohr lzd
 * @description
 * @date 2020/9/7
 */
@RestController
public class WaterController {
    private Logger log = LoggerFactory.getLogger(SmokeController.class);
    @Autowired
    private WaterService waterService;
    @GetMapping("/water/all")
    public List<WaterEntity> type() {
        List<WaterEntity> waterTypeList = waterService.getWatersType();
        return waterTypeList;
    }
    @GetMapping("/water/capacity")
    public List<WaterEntity> capacity() {
        List<WaterEntity> waterCapacityList = waterService.getWatersCapacity();
        return waterCapacityList;
    }
    @GetMapping("/waters")
    public PageResult list(String waterCapacity,String waterType,String waterName, int pageSize, int page) {
        PageResult PageResult = new PageResult();
        PageResult.setData(waterService.getWatersList(waterCapacity,waterType,waterName, pageSize, pageSize*page));
        PageResult.setTotalCount(waterService.getWatersSize(waterCapacity,waterType,waterName, pageSize, pageSize*page));
        return PageResult;
    }
    @PostMapping("/waters/water")
    public WaterEntity add(@RequestBody WaterEntity waterEntity, Model model) {
        waterService.insertWater(waterEntity);
        List<WaterEntity> waterCapacity =  waterService.getWatersCapacity();
        model.addAttribute("waterCapacity", waterCapacity);
        log.debug("The method is ending");
        return waterEntity;
    }
    @DeleteMapping("/waters")
    public List<String> delete(@RequestBody List<String> groupId) {
        waterService.deleteWaterById(groupId);
        System.out.println("我是删除");
        return groupId;
    }
    @PutMapping("/waters/{waterId}")
    public WaterEntity edit(@RequestBody WaterEntity waterEntity, @PathVariable int waterId) {
            waterService.updateWaterById(waterEntity);
        log.debug("The method is ending");
        return waterEntity;
    }
}
