package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.AppEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.AppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/7
 */
@RestController
public class AppController {
    private Logger log = LoggerFactory.getLogger(AppController.class);

    @Resource(name = "appServiceImpl")
    private AppService appService;

    @GetMapping("/apps")
    public PageResult appsList(int pageSize, String appOnline, String appType, int page) {
        System.out.println("我走了这个方法");
        PageResult pageResult = new PageResult();
        pageResult.setData(appService.appsList(pageSize, appOnline,appType,page * pageSize));
        pageResult.setTotalCount(appService.appsSize(pageSize, appOnline,appType,page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }

    @PostMapping("/apps/app")
    public AppEntity insertApp(@RequestBody AppEntity appEntity) {
        appService.insertApp(appEntity);
        log.debug("The method is ending");
        return appEntity;
    }

    @PutMapping("/apps/{appId}")
    public AppEntity updateApp(@RequestBody AppEntity appEntity, @PathVariable int appId) {
        if (appEntity.getAppId() == appId) {
            appService.updateApp(appEntity);
        }
        log.debug("The method is ending");
        return appEntity;
    }

    @DeleteMapping("/apps")
    public List<String> deleteApps(@RequestBody List<String> groupId) {
        appService.deleteApps(groupId);
        return groupId;
    }

    @GetMapping("/apps/appOnline")
    public List<AppEntity>appsByLine() {
        return appService.appsByLine();
    }
}
