package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.DrinksEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.model.sys.WaterEntity;
import com.asianrapid.service.sys.DrinksService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @autohr lzd
 * @description
 * @date 2020/9/9
 */
@RestController
public class DrinksController {
    private Logger log = LoggerFactory.getLogger(DrinksController.class);
    @Autowired
    private DrinksService drinksService;
    @GetMapping("/drinks/type")
    public List<DrinksEntity> type() {
        List<DrinksEntity> drinksTypeList = drinksService.getDrinksType();
        return drinksTypeList;
    }
    @GetMapping("/drinks/capacity")
    public List<DrinksEntity> capacity() {
        List<DrinksEntity> drinksCapacityList = drinksService.getDrinksCapacity();
        return drinksCapacityList;
    }
    @GetMapping("/drinks")
    public PageResult list(String drinksCapacity, String drinksType, String drinksName, int pageSize, int page) {
        PageResult PageResult = new PageResult();
        PageResult.setData(drinksService.getDrinksList(drinksCapacity,drinksType,drinksName, pageSize, pageSize*page));
        PageResult.setTotalCount(drinksService.getDrinksListSize(drinksCapacity,drinksType,drinksName, pageSize, pageSize*page));
        return PageResult;
    }

    @PostMapping("/drinks/drink")
    public DrinksEntity add(@RequestBody DrinksEntity drinksEntity) {
        drinksService.insertDrinks(drinksEntity);
        log.debug("The method is ending");
        return drinksEntity;
    }
    @DeleteMapping("/drinks")
    public List<String> delete(@RequestBody List<String> groupId) {
        drinksService.deleteDrinksById(groupId);
        return groupId;
    }
    @PutMapping("/drinks/{drinksId}")
    public DrinksEntity edit(@RequestBody DrinksEntity drinksEntity, @PathVariable int drinksId) {
        drinksService.updateDrinksById(drinksEntity);
        log.debug("The method is ending");
        return drinksEntity;
    }
}
