package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.PageResult;
import com.asianrapid.model.sys.PaintingEntity;
import com.asianrapid.service.sys.PaintingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr lx
 * @description
 * @date 2020/9/7
 */
@RestController
public class PaintingController {

    private Logger log = LoggerFactory.getLogger(PaintingController.class);

    @Resource(name = "PaintingServiceImpl")
    private PaintingService paintingService;

    /**
     * 获取画作表数据
     *
     * @param pageSize
     * @param page
     * @return
     */
    @GetMapping("/painting")
    public PageResult paintingList(String paintingName, String paintingType, int pageSize, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(paintingService.paintingList(paintingName,paintingType,pageSize, page * pageSize));
        pageResult.setTotalCount(paintingService.paintingSize(paintingName,paintingType,pageSize, page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }
    /**
     * 新建角色信息
     *
     * @param paintingEntity
     * @return
     */
    @PostMapping("/painting/insert")
    public PaintingEntity insertPainting(@RequestBody PaintingEntity paintingEntity) {
        paintingService.insertpainting(paintingEntity);
        log.debug("The method is ending");
        return paintingEntity;
    }
    /**
     * 获取画作表下拉框
     */
    @GetMapping("/paintings")
    public List<PaintingEntity> paintingTypeList() {
        return paintingService.paintingTypeList();
    }
    /**
     * 删除菜单信息
     *
     * @param groupId
     * @return
     */
    @DeleteMapping("/painting/delete")
    public List<String> deletePainting(@RequestBody List<String> groupId) {
        paintingService.deletePainting(groupId);
        return groupId;
    }
    /**
     * 更新角色信息
     *
     * @param paintingEntity
     * @param paintingId
     * @return
     */
    @PutMapping("/painting/{paintingId}")
    public PaintingEntity updateRole(@RequestBody PaintingEntity paintingEntity, @PathVariable int paintingId) {
        if (paintingEntity.getPaintingId() == paintingId) {
            paintingService.updatePainting(paintingEntity);
        }
        log.debug("The method is ending");
        return paintingEntity;
    }

    @GetMapping("/painting/{paintingName}")
    public PaintingEntity paintingGet(@PathVariable String paintingName) {
        PaintingEntity paintingEntity = paintingService.getPaintingEntity(paintingName);
        log.debug("The method is ending");
        return paintingEntity;
    }
}
