package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.ApEntity;

import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.ApService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/9
 */
@RestController
public class ApController {
    private Logger log = LoggerFactory.getLogger(ApController.class);

    @Resource(name = "apServiceImpl")
    private ApService apService;

    @GetMapping("/aps")
    public PageResult apsList(int pageSize, String apOnline, String apType, int page) {
        System.out.println("我走了这个方法");
        PageResult pageResult = new PageResult();
        pageResult.setData(apService.apsList(pageSize, apOnline,apType,page * pageSize));
        pageResult.setTotalCount(apService.apsSize(pageSize, apOnline,apType,page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }

    @PostMapping("/aps/ap")
    public ApEntity insertAp(@RequestBody ApEntity apEntity) {
        apService.insertAp(apEntity);
        log.debug("The method is ending");
        return apEntity;
    }

    @PutMapping("/aps/{apId}")
    public ApEntity updateAp(@RequestBody ApEntity apEntity, @PathVariable int apId) {
        if (apEntity.getApId() == apId) {
            apService.updateAp(apEntity);
        }
        log.debug("The method is ending");
        return apEntity;
    }

    @DeleteMapping("/aps")
    public List<String> deleteAps(@RequestBody List<String> groupId) {
        apService.deleteAps(groupId);
        return groupId;
    }

    @GetMapping("/aps/apOnline")
    public List<ApEntity>apsByLine() {
        return apService.apsByLine();
    }
}
