package com.asianrapid.controller.sys;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/4
 */
import com.asianrapid.model.sys.MenuEntity;
import com.asianrapid.model.sys.UserEntity;
import com.asianrapid.service.sys.DogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import com.asianrapid.model.sys.DogEntity;
import com.asianrapid.model.sys.PageResult;
import java.util.List;

@RestController
public class DogController {

    private Logger log = LoggerFactory.getLogger(DogController.class);

    @Resource(name = "dogServiceImpl")
    private DogService dogService;

    @GetMapping("/dogs/fur")
    public List<DogEntity> dogList() {
        return dogService.dogList();
    }


    @GetMapping("/dogs")
    public PageResult dogsList(int pageSize, String fur, String name, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(dogService.dogsList(pageSize,  fur, name,page * pageSize));
        pageResult.setTotalCount(dogService.dogsSize(pageSize,  fur, name,page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }

    @PostMapping("/dogs/dog")
    public DogEntity insertDog(@RequestBody DogEntity dogEntity) {
        dogService.insertDog(dogEntity);
        log.debug("The method is ending");
        return dogEntity;
    }

    @PutMapping("/dogs/{id}")
    public DogEntity updateDog(@RequestBody DogEntity dogEntity, @PathVariable int id) {
        if (dogEntity.getId() == id) {
            dogService.updateDog(dogEntity);
        }
        log.debug("The method is ending");
        return dogEntity;
    }

    @DeleteMapping("/dogs")
    public List<String> deleteDogs(@RequestBody List<String> groupId) {
        System.out.println("111111111");
        dogService.deleteDogs(groupId);
        return groupId;
    }

}
