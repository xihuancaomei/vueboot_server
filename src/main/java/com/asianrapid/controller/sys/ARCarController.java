package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.ARCarEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.ARCarService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/9
 */
@RestController
public class ARCarController {

    @Resource(name = "arCarServiceImpl")
    private ARCarService arCarService;

    @GetMapping("/arcar")
    public PageResult getCarList(int pageSize, int page,String carType,String carTrans, String carOrign){
        PageResult pageResult = new PageResult();
        pageResult.setData(arCarService.getCarList(pageSize, page*pageSize,carType,carTrans,carOrign));
        pageResult.setTotalCount(arCarService.getCarListCount(carType,carTrans,carOrign));
        return pageResult;
    }

    @GetMapping("/arcar/getTransList")
    public List<String> getCarTransList(){
        return arCarService.getCarTransList();
    }

    @GetMapping("/arcar/getOrignList")
    public List<String> getCarOrignList(){
        return arCarService.getCarOrignList();
    }

    @PostMapping("/arcar/insert")
    public void insertCar(@RequestBody ARCarEntity arCar){
        arCarService.insertCar(arCar);
    }

    @PutMapping("/arcar/{carId}")
    public void updateCar(@RequestBody  ARCarEntity arCar,@PathVariable Long carId){
        arCar.setCarId(carId);
        arCarService.updateCar(arCar);
    }

    @DeleteMapping("/arcar/delete")
    public void deleteCars(@RequestBody Long[] carIds){
        arCarService.deleteCars(carIds);
    }
}
