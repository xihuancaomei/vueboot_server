package com.asianrapid.controller.sys;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/4
 */

import com.asianrapid.model.sys.CartoonEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.CartoonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class CartoonController {

    private Logger log = LoggerFactory.getLogger(CartoonController.class);

    @Resource(name = "cartoonServiceImpl")
    private CartoonService cartoonService;

    @GetMapping("/cartoons/cartoonCategory")
    public List<CartoonEntity> cartoonList() {
        return cartoonService.cartoonList();
    }


    @GetMapping("/cartoons")
    public PageResult cartoonsList(int pageSize, String cartoonCategory, String cartoonName, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(cartoonService.cartoonsList(pageSize,  cartoonCategory, cartoonName,page * pageSize));
        pageResult.setTotalCount(cartoonService.cartoonsSize(pageSize,  cartoonCategory, cartoonName,page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }

    @PostMapping("/cartoons/cartoon")
    public CartoonEntity insertCartoon(@RequestBody CartoonEntity cartoonEntity) {
        cartoonService.insertCartoon(cartoonEntity);
        log.debug("The method is ending");
        return cartoonEntity;
    }

    @PutMapping("/cartoons/{cartoonId}")
    public CartoonEntity updateCartoon(@RequestBody CartoonEntity cartoonEntity, @PathVariable int cartoonId) {
        if (cartoonEntity.getCartoonId() == cartoonId) {
            cartoonService.updateCartoon(cartoonEntity);
        }
        log.debug("The method is ending");
        return cartoonEntity;
    }

    @DeleteMapping("/cartoons")
    public List<String> deleteCartoons(@RequestBody List<String> groupId) {
        cartoonService.deleteCartoons(groupId);
        return groupId;
    }

}
