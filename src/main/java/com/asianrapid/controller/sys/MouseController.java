package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.MouseEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.MouseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class MouseController {

    private Logger log = LoggerFactory.getLogger(MouseController.class);

    @Resource(name = "mouseServiceImpl")
    private MouseService mouseService;

    @GetMapping("/mouses")
    public PageResult mousesList(String mouseName, String mouseBrand,  String mouseCat, int pageSize, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(mouseService.mousesList(mouseName, mouseBrand, mouseCat, pageSize, page * pageSize));
        pageResult.setTotalCount(mouseService.mousesSize(mouseName, mouseBrand, mouseCat, pageSize, page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }

    @GetMapping("/mouses/all")
    public List<MouseEntity> allRoles(){
        return mouseService.allMouses();
    }

    @PostMapping("/mouses/mouse")
    public MouseEntity insertMouse(@RequestBody MouseEntity mouseEntity) {
        mouseService.insertMouse(mouseEntity);
        log.debug("The method is ending");
        return mouseEntity;
    }

    @PutMapping("/mouses/{mouseId}")
    public MouseEntity updateMouse(@RequestBody MouseEntity mouseEntity, @PathVariable int mouseId) {
        if (mouseEntity.getMouseId() == mouseId) {
            mouseService.updateMouse(mouseEntity);
        }
        log.debug("The method is ending");
        return mouseEntity;
    }

    @DeleteMapping("/mouses")
    public List<String> deleteMouses(@RequestBody List<String> groupId) {
        System.out.println(groupId);
        mouseService.deleteMouses(groupId);
        return groupId;
    }

}
