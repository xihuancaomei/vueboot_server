package com.asianrapid.controller.sys;


import com.asianrapid.model.sys.PageResult;
import com.asianrapid.model.sys.PlantEntity;
import com.asianrapid.service.sys.PlantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class PlantController {

	private Logger log = LoggerFactory.getLogger(PlantController.class);

	@Resource(name = "plantServiceImpl")
	private PlantService plantService;

	@GetMapping("/plants/plantType")
	public List<PlantEntity>plantTypeList(){
		List<PlantEntity> plantEntity = plantService.plantTypeList();
		return plantEntity;
	}

	@GetMapping("/plants")
	public PageResult plantsList(int pageSize, String plantType, String plantName, int page) {
		PageResult pageResult = new PageResult();
		pageResult.setData(plantService.plantsList(pageSize, plantType, plantName, page * pageSize));
		pageResult.setTotalCount(plantService.plantsSize(pageSize, plantType, plantName, page * pageSize));
		log.debug("The method is ending");
		return pageResult;
	}

	@PostMapping("/plants/plant")
	public PlantEntity insertPlant(@RequestBody PlantEntity plantEntity) {
		plantService.insertPlant(plantEntity);
		log.debug("The method is ending");
		return plantEntity;
	}

	@PutMapping("/plants/{plantId}")
	public PlantEntity updatePlant(@RequestBody PlantEntity plantEntity, @PathVariable int plantId) {
		if (plantEntity.getPlantId() == plantId) {
			plantService.updatePlant(plantEntity);
		}
		log.debug("The method is ending");
		return plantEntity;
	}

	@DeleteMapping("/plants")
	public List<String> deletePlants(@RequestBody List<String> groupId) {
		plantService.deletePlants(groupId);
		return groupId;
	}
}