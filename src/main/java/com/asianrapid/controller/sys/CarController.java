package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.CarEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.service.sys.CarService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/3
 */
@RestController
public class CarController {

    private Logger log = LoggerFactory.getLogger(CarController.class);

    @Resource(name = "carServiceImpl")
    private CarService carService;

    @GetMapping("/car")
    public PageResult getCarList(String carType,String carTransmission,String carOrign, int pageSize, int page){
        PageResult pageResult = new PageResult();
        pageResult.setData(carService.getCarList(carType,carTransmission,carOrign,pageSize,page*pageSize));
        pageResult.setTotalCount(carService.getCarListSize(carType,carTransmission,carOrign,pageSize,page*pageSize));
        log.debug("The method is ending");
        System.out.println(pageResult);
        return pageResult;
    }

    @GetMapping("/car/carType")
    public List<CarEntity> getCarTypeList(){
        System.out.println("我是下拉框方法");
        System.out.println(carService.getCarTypeList());
        return carService.getCarTypeList();
    }

    @PostMapping("/car/insert")
    public void insertCar(@RequestBody CarEntity carEntity){
        carService.insertCar(carEntity);
        log.debug("The method is ending");
        System.out.println(carEntity);
    }

    @PutMapping("/car/{carId}")
    public void updateCar(@RequestBody CarEntity carEntity, @PathVariable Long carId){
        carEntity.setCarId(carId);
        carService.updateCar(carEntity);
    }

    @DeleteMapping("/car")
    public void deleteCar(@RequestBody Long[] carIds){
        System.out.println("我走了删除方法");
        System.out.println(carIds[0]);
        carService.deleteCar(carIds);
    }
}
