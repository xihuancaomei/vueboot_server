package com.asianrapid.controller.sys;

import com.asianrapid.model.sys.BookEntity;
import com.asianrapid.model.sys.PageResult;
import com.asianrapid.model.sys.UserEntity;
import com.asianrapid.service.sys.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class BookController {

	private Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    /**
     * 获取book表数据
     */
    @GetMapping("/books")
    public PageResult usersList(String bookName, String bookAuthor, String bookPress,int pageSize, int page) {
        PageResult pageResult = new PageResult();
        pageResult.setData(bookService.booksList(bookName, bookAuthor, bookPress, pageSize, page * pageSize));
        pageResult.setTotalCount(bookService.booksSize(bookName, bookAuthor, bookPress, pageSize, page * pageSize));
        log.debug("The method is ending");
        return pageResult;
    }
    /**
     * 获取全部下拉数据
     */
    @GetMapping("/books/all")
    public List<BookEntity> allPressList(){
        List<BookEntity> bookList = bookService.selectBookList();
        return bookList;
    }
    /**
     * 新增book表数据
     */
    @PostMapping("/books/add")
    public BookEntity insertBook(@RequestBody BookEntity bookEntity) {
        bookService.insertBook(bookEntity);
        log.debug("The method is ending");
        return bookEntity;
    }
    /**
     * 更新book信息
     */
    @PutMapping("/books/{id}")
    public BookEntity updateBook(@RequestBody BookEntity bookEntity, @PathVariable Long id) {
        if (bookEntity.getBookId() == id) {
            bookService.updateBook(bookEntity);
        }
        log.debug("The method is ending");
        return bookEntity;
    }
    /**
     * 删除book信息
     */
    @DeleteMapping("/books")
    public List<String> deleteBooks(@RequestBody List<String> groupId) {
        bookService.deleteBooks(groupId);
        return groupId;
    }

}
