package com.asianrapid.dao;

import com.asianrapid.model.sys.Customer;
import com.asianrapid.model.sys.CustomerOrderDto;
import com.asianrapid.model.sys.Order;
import com.asianrapid.model.sys.QueryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface CustomerDao {

    public List<String> selectOrderNameList();

    public List<CustomerOrderDto> customersList(@Param("customerName") String customerName, @Param("orderId") String orderId, @Param("name") String name, @Param("pageSize") int pageSize, @Param("start") int start);

    public Integer customerSize(@Param("customerName") String customerName, @Param("orderId") String orderId, @Param("name") String name, @Param("pageSize") int pageSize, @Param("start") int start);

    public void insertCustomer(@Param("customerOrderDto") CustomerOrderDto CustomerOrderDto);

    public void insertOrder(@Param("customerOrderDto") CustomerOrderDto CustomerOrderDto);

    public Long selectCustomerId();

    public Long selectOrderId();

    public void insertCustomerOrderId(@Param("queryVo") QueryVo queryVo);

    public void updateCustomer(@Param("customerOrderDto") CustomerOrderDto customerOrderDto);

    public void updateOrder(@Param("order") Order order);

    public void deleteCustomers(@Param("groupId") List<String> groupId);
}
