package com.asianrapid.dao;

import com.asianrapid.model.sys.GameEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;
@Mapper
public interface GameDao {
    /**
     * 获取game列表
     */
    public ArrayList<GameEntity> gamesList(@Param("gameName") String gameName,@Param("gameType") String gameType,@Param("pageSize") int pageSize, @Param("start") int start);

    /**
     * 获取game列表的总量
     */
    public Integer gamesSize(@Param("gameName") String gameName,@Param("gameType") String gameType,@Param("pageSize") int pageSize,@Param("start") int start);
    /**
     * 新建角色信息
     * @param gameEntity
     */
    public void insertGame(@Param("gameEntity") GameEntity gameEntity);

    /**
     * 更新角色信息
     * @param gameEntity
     */
    public void updateRole(@Param("gameEntity") GameEntity gameEntity);

    /**
     * 删除角色信息
     * @param groupId
     */
    public void deleteRoles(@Param("groupId") List<String> groupId);

    /**
     * 通过登录名拿到用户信息
     * @return
     */
    public GameEntity getGameEntityByGameName(@Param("gameName") String gameName);

    public List<GameEntity> gameTypeList();
}
