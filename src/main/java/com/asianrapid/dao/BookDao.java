package com.asianrapid.dao;

import com.asianrapid.model.sys.BookEntity;
import com.asianrapid.model.sys.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface BookDao {

	/**
	 * 获取book列表
	 */
	public ArrayList<BookEntity> selectBooksList(String bookName, String bookAuthor, String bookPress, int pageSize, int start);
	/**
	 * 获取book列表的总量
	 */
	public Integer selectBooksSize(String bookName, String bookAuthor, String bookPress, int pageSize, int start);


    public List<BookEntity> selectBookList();

    /**
     * 新增book信息
     */

	public void insertBook( BookEntity bookEntity);

	/**
	 * 更新book信息
	 */

	public void updateBook(BookEntity bookEntity);

    /**
     * 删除book信息
     */

	public void deleteBooks(@Param("groupId") List<String> groupId);
	
}
