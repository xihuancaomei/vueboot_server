package com.asianrapid.dao;

import com.asianrapid.model.sys.CarEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface CarDao {
    ArrayList<CarEntity> getCarList(@Param("carType") String carType,@Param("carTransmission") String carTransmission,@Param("carOrign") String carOrign,@Param("pageSize") int pageSize, @Param("start") int start);
    Integer getCarListSize(@Param("carType") String carType,@Param("carTransmission") String carTransmission,@Param("carOrign") String carOrign,@Param("pageSize") int pageSize,@Param("start") int start);
    void insertCar(CarEntity carEntity);
    void updateCar(CarEntity carEntity);
    void deleteCar(@Param("carIds") Long[] carIds);
    List<CarEntity> getCarTypeList();
}
