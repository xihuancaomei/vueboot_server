package com.asianrapid.dao;

import com.asianrapid.model.sys.DrinksEntity;
import com.asianrapid.model.sys.SmokeEntity;
import com.asianrapid.model.sys.WaterEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface DrinksDao {
    public ArrayList<DrinksEntity> getDrinksList(@Param("drinksCapacity") String drinksCapacity, @Param("drinksType") String drinksType, @Param("drinksName") String drinksName, @Param("pageSize") int pageSize, @Param("start") int start);
    public Integer getDrinksListSize(@Param("drinksCapacity") String drinksCapacity,@Param("drinksType") String drinksType, @Param("drinksName") String drinksName, @Param("pageSize") int pageSize, @Param("start") int start);
    public List<DrinksEntity> getDrinksType();
    public List<DrinksEntity> getDrinksCapacity();
    public void insertDrinks(@Param("drinksEntity") DrinksEntity drinksEntity);
    public void deleteDrinksById(@Param("groupId") List<String> groupId);
    public void updateDrinksById(@Param("drinksEntity")DrinksEntity drinksEntity);
}
