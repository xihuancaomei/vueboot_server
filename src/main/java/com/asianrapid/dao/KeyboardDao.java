package com.asianrapid.dao;

import com.asianrapid.model.sys.Keyboard;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface KeyboardDao {

    public String[] selectBrandList();

    public String[] selectCatList();

    public List<Keyboard> keyboardsList(@Param("keyboardName")String keyboardName, @Param("keyboardBrand")String keyboardBrand, @Param("keyboardCat")String keyboardCat, @Param("pageSize") int pageSize, @Param("start") int start);

    public Integer keyboardSize(@Param("keyboardName")String keyboardName, @Param("keyboardBrand")String keyboardBrand, @Param("keyboardCat")String keyboardCat, @Param("pageSize") int pageSize, @Param("start") int start);

    public void insertKeyboard(@Param("keyboard") Keyboard keyboard);

    public void updateKeyboard(@Param("keyboard") Keyboard keyboard);

    public void deleteKeyboards(@Param("groupId") List<String> groupId);

}
