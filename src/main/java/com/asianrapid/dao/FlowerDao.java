package com.asianrapid.dao;


import com.asianrapid.model.sys.FlowerEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FlowerDao {

	public List<FlowerEntity> getFlowerListById(@Param("ids") String[] ids);
	public List<FlowerEntity> flowersList(@Param("pageSize") int pageSize, @Param("flowerType") String flowerType, @Param("flowerName") String flowerName, @Param("start") int start);

	public Integer flowersSize(@Param("pageSize") int pageSize, @Param("flowerType") String flowerType, @Param("flowerName") String flowerName, @Param("start") int start);
	public void insertFlower(@Param("flowerEntity") FlowerEntity flowerEntity);

	public void updateFlower(@Param("flowerEntity") FlowerEntity flowerEntity);

	public void deleteFlowers(@Param("groupId") List<String> groupId);

	public List<FlowerEntity> flowerTypeList();

}