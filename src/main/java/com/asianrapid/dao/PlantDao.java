package com.asianrapid.dao;



import com.asianrapid.model.sys.PlantEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PlantDao {

	public List<PlantEntity> getPlantListById(@Param("ids") String[] ids);
	public List<PlantEntity> plantsList(@Param("pageSize") int pageSize, @Param("plantType") String plantType, @Param("plantName") String plantName, @Param("start") int start);

	public Integer plantsSize(@Param("pageSize") int pageSize, @Param("plantType") String plantType, @Param("plantName") String plantName, @Param("start") int start);
	public void insertPlant(@Param("plantEntity") PlantEntity plantEntity);

	public void updatePlant(@Param("plantEntity") PlantEntity plantEntity);

	public void deletePlants(@Param("groupId") List<String> groupId);

	public List<PlantEntity> plantTypeList();

}