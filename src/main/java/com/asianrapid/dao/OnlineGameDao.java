package com.asianrapid.dao;

import com.asianrapid.model.sys.BookEntity;
import com.asianrapid.model.sys.OnLineGameDictEntity;
import com.asianrapid.model.sys.OnlineGameEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface OnlineGameDao {

    /**
     * 获取online_game表
     */
    public ArrayList<OnlineGameEntity> selectOnlineGamesList(String gameName, String gameType, String gameOperation, int pageSize, int start);

    /**
     * 获取online_game表的总数
     */
    public Integer selectOnlineGamesSize(String gameName, String gameType, String gameOperation, int pageSize, int start);

    /**
     * 新增online_game数据
     */
    public void insertOnlineGames(OnlineGameEntity onlineGameEntity);

    /**
     * 更新online_game信息
     */

    public void updateOnlineGames(OnlineGameEntity onlineGameEntity);

    /**
     * 删除online_game信息
     */

    public void deleteOnlineGames(@Param("groupId") List<String> groupId);

    public List<OnLineGameDictEntity> selectTypeList();

    public List<OnLineGameDictEntity> selectOperationList();
}
