package com.asianrapid.dao;

import com.asianrapid.model.sys.MouseEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface MouseDao {

    public ArrayList<MouseEntity> mousesList(@Param("mouseName")String mouseName, @Param("mouseBrand")String mouseBrand, @Param("mouseCat")String mouseCat, @Param("pageSize") int pageSize, @Param("start") int start);

    public Integer mousesSize(@Param("mouseName")String mouseName, @Param("mouseBrand")String mouseBrand, @Param("mouseCat")String mouseCat, @Param("pageSize") int pageSize, @Param("start") int start);

    public List<MouseEntity> allMouses();

    public void insertMouse(@Param("mouseEntity") MouseEntity mouseEntity);

    public void updateMouse(@Param("mouseEntity") MouseEntity mouseEntity);

    public void deleteMouses(@Param("groupId") List<String> groupId);

}
