package com.asianrapid.dao;

import com.asianrapid.model.sys.WaterEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface WaterDao {
    public List<WaterEntity> getWatersList(@Param("waterCapacity")String waterCapacity,@Param("waterType")String waterType,@Param("waterName")String waterName, @Param("pageSize")int pageSize, @Param("start")int start);
    public Integer getWatersSize(@Param("waterCapacity")String waterCapacity,@Param("waterType")String waterType,@Param("waterName")String waterName, @Param("pageSize")int pageSize, @Param("start")int start);
    public List<WaterEntity> getWatersType();
    public List<WaterEntity> getWatersCapacity();
    public void insertWater(@Param("waterEntity") WaterEntity waterEntity);
    public void deleteWaterById(@Param("groupId") List<String> groupId);
    public void updateWaterById(@Param("waterEntity")WaterEntity waterEntity);
}
