package com.asianrapid.dao;

import com.asianrapid.model.sys.StudentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface StudentDao {
    List<StudentEntity> getStdList(@Param("stdName") String stdName,@Param("stdNumber") String stdNumber,
                                   @Param("stdRoom") String stdRoom,@Param("stdClass") String stdClass,
                                   @Param("stdTeacher") String stdTeacher,@Param("pageSize") int pageSize,
                                   @Param("start") int start);
    Integer getStdListCount(@Param("stdName") String stdName,@Param("stdNumber") String stdNumber,
                            @Param("stdRoom") String stdRoom,@Param("stdClass") String stdClass,
                            @Param("stdTeacher") String stdTeacher,@Param("pageSize") int pageSize,
                            @Param("start") int start);
    List<String> getStdClassList();
    List<String> getStdTeacherList();
    void insertStd(StudentEntity std);
    void updateStd(StudentEntity std);
    void deleteStd(@Param("stdIds") Long[] stdIds);
}
