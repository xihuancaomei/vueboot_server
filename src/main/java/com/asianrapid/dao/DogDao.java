package com.asianrapid.dao;

import com.asianrapid.model.sys.DogEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DogDao {


    public List<DogEntity> getDogListById(@Param("ids") String[] ids);
    public List<DogEntity> dogsList(@Param("pageSize") int pageSize, @Param("fur") String fur, @Param("name") String name,
                                    @Param("start") int start);

    public Integer dogsSize(@Param("pageSize") int pageSize, @Param("fur") String fur, @Param("name") String name,
                            @Param("start") int start);
    public void insertDog(@Param("dogEntity") DogEntity dogEntity);

    public void updateDog(@Param("dogEntity") DogEntity dogEntity);

    public void deleteDogs(@Param("groupId") List<String> groupId);

    public List<DogEntity> dogList();

}
