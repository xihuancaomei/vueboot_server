package com.asianrapid.dao;

import com.asianrapid.model.sys.Film;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.ArrayList;
import java.util.List;

@Mapper
public interface FilmDao {

    /**
     列表
     */
    public ArrayList<Film> filmList(@Param("pageSize") int pageSize, @Param("start") int start,@Param("type") String type,@Param("name") String name);

    /**
     列表的总量
     */
    public Integer filmSize(@Param("pageSize") int pageSize,@Param("start") int start,@Param("type") String type,@Param("name") String name);

    /**
     新增电影
     */
    public void insertFilm(@Param("filmEntity") Film filmEntity);

    /**
     返显
     */
    public List<Film> allFilm();

    /**
     修改电影
     */
    public void updateFilm(@Param("filmEntity") Film filmEntity);

    /**
     删除电影
     */
    public void deleteFilm(@Param("groupId") List<String> groupId);

    public List<Film> filmByType();

}
