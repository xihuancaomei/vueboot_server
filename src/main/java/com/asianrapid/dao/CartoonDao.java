package com.asianrapid.dao;

import com.asianrapid.model.sys.CartoonEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CartoonDao {


    public List<CartoonEntity> getCartoonListById(@Param("ids") String[] ids);
    public List<CartoonEntity> cartoonsList(@Param("pageSize") int pageSize, @Param("cartoonCategory") String cartoonCategory, @Param("cartoonName") String cartoonName,
                                    @Param("start") int start);

    public Integer cartoonsSize(@Param("pageSize") int pageSize, @Param("cartoonCategory") String cartoonCategory, @Param("cartoonName") String cartoonName,
                            @Param("start") int start);
    public void insertCartoon(@Param("cartoonEntity") CartoonEntity cartoonEntity);

    public void updateCartoon(@Param("cartoonEntity") CartoonEntity cartoonEntity);

    public void deleteCartoons(@Param("groupId") List<String> groupId);

    public List<CartoonEntity> cartoonList();

}
