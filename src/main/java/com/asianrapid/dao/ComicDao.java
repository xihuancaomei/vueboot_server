package com.asianrapid.dao;

import com.asianrapid.model.sys.ComicEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ComicDao {


    public List<ComicEntity> getComicListById(@Param("ids") String[] ids);
    public List<ComicEntity> comicsList(@Param("pageSize") int pageSize, @Param("comicCategory") String comicCategory, @Param("comicName") String comicName,
                                            @Param("start") int start);

    public Integer comicsSize(@Param("pageSize") int pageSize, @Param("comicCategory") String comicCategory, @Param("comicName") String comicName,
                                @Param("start") int start);
    public void insertComic(@Param("comicEntity") ComicEntity comicEntity);

    public void updateComic(@Param("comicEntity") ComicEntity comicEntity);

    public void deleteComics(@Param("groupId") List<String> groupId);

    public List<ComicEntity> comicList();

}
