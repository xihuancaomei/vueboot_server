package com.asianrapid.dao;

import com.asianrapid.model.sys.MenuEntity;
import com.asianrapid.model.sys.ToyEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ToyDao {

	public List<ToyEntity> getToyListById(@Param("ids") String[] ids);
	public List<ToyEntity> toysList(@Param("pageSize") int pageSize,@Param("name") String toyName,@Param("effect") String effect,@Param("start") int start);

	public Integer toysSize(@Param("pageSize") int pageSize,@Param("name") String name,@Param("effect") String effect,@Param("start") int start);
	public void insertToy(@Param("toyEntity") ToyEntity toyEntity);

	public void updateToy(@Param("toyEntity") ToyEntity toyEntity);

	public void deleteToys(@Param("groupId") List<String> groupId);


	public List<ToyEntity> toysByPopular(@Param("popular") int popular);

	public List<ToyEntity> nameList();

}
