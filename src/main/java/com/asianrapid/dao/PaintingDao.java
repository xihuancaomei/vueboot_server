package com.asianrapid.dao;

import com.asianrapid.model.sys.PaintingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface PaintingDao {

    /**
     * 获取画作列表
     */
    public ArrayList<PaintingEntity> paintingList(@Param("paintingName") String paintingName, @Param("paintingType") String paintingType, @Param("pageSize") int pageSize, @Param("start") int start);

    /**
     * 获取画作列表的总量
     */
    public Integer paintingSize(@Param("paintingName") String paintingName,@Param("paintingType") String paintingType,@Param("pageSize") int pageSize,@Param("start") int start);
    /**
     * 新建画作信息
     * @param paintingEntity
     */
    public void insertpainting(@Param("paintingEntity") PaintingEntity paintingEntity);
    /**
     * 获取画作表下拉框
     */
    public List<PaintingEntity> paintingTypeList();
    /**
     * 删除画作信息    updatePainting
     * @param groupId
     */
    public void deletePainting(@Param("groupId") List<String> groupId);
    /**
     * 更新画作信息
     * @param paintingEntity
     */
    public void updatePainting(@Param("paintingEntity") PaintingEntity paintingEntity);
    /**
     * 通过画作名拿到画作信息
     * @return
     */
    public PaintingEntity getPaintingEntity(@Param("paintingName") String paintingName);
}
