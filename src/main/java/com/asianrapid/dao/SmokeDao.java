package com.asianrapid.dao;

import com.asianrapid.model.sys.SmokeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.ArrayList;
import java.util.List;
@Mapper
public interface SmokeDao {
    public ArrayList<SmokeEntity> getSmokeList(@Param("smokeTar") String smokeTar,@Param("smokeType") String smokeType, @Param("smokeName") String smokeName, @Param("pageSize") int pageSize, @Param("start") int start);
    public Integer getSmokeListSize(@Param("smokeTar") String smokeTar,@Param("smokeType") String smokeType, @Param("smokeName") String smokeName, @Param("pageSize") int pageSize, @Param("start") int start);
    public void insertSmoke(@Param("smokeEntity") SmokeEntity smokeEntity);
    public void updateSmokes(@Param("smokeEntity") SmokeEntity smokeEntity);
    public void deleteSmoke(@Param("groupId") List<String> groupId);
    public List<SmokeEntity> getSmokeTypeList();
}
