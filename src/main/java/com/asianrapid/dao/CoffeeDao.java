package com.asianrapid.dao;

import com.asianrapid.model.sys.CoffeeEntity;
import com.asianrapid.model.sys.MenuEntity;
import com.asianrapid.model.sys.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @autohr gwb
 * @description
 * @date 2020/9/3
 */
@Mapper
public interface CoffeeDao {

    public List<CoffeeEntity> getCoffeeListById(@Param("ids") String[] ids);


    public List<CoffeeEntity> coffeesList(@Param("pageSize") int pageSize, @Param("coffeeName") String coffeeName,@Param("coffeeAddSugar") String coffeeAddSugar, @Param("start") int start);


    public Integer coffeesSize(@Param("pageSize") int pageSize,@Param("coffeeName") String coffeeName, @Param("coffeeAddSugar") String coffeeAddSugar,@Param("start") int start);


    public void insertCoffee(@Param("coffeeEntity") CoffeeEntity coffeeEntity);


    public void updateCoffee(@Param("coffeeEntity") CoffeeEntity coffeeEntity);


    public void deleteCoffees(@Param("groupId") List<String> groupId);

    public List<CoffeeEntity> coffeesByName();

}
