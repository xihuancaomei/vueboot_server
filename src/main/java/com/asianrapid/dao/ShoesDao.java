package com.asianrapid.dao;

import com.asianrapid.model.sys.ShoesEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface ShoesDao {

    /**
     列表
     */
    public ArrayList<ShoesEntity> shoesList(@Param("pageSize") int pageSize, @Param("start") int start, @Param("type") String type, @Param("brand") String brand);

    /**
     列表的总量
     */
    public Integer shoesSize(@Param("pageSize") int pageSize,@Param("start") int start,@Param("type") String type,@Param("brand") String brand);

    /**
     新增电影
     */
    public void insertShoes(@Param("shoesEntity") ShoesEntity shoesEntity);

    /**
     返显
     */
    public List<ShoesEntity> allShoes();

    /**
     修改电影
     */
    public void updateShoes(@Param("shoesEntity") ShoesEntity shoesEntity);

    /**
     删除电影
     */
    public void deleteShoes(@Param("groupId") List<String> groupId);

    public List<ShoesEntity> shoesByType();
}
